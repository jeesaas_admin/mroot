/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]
<http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>
<http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.model.entity.cms


import com.baomidou.mybatisplus.activerecord.Model
import com.baomidou.mybatisplus.annotations.TableId
import com.baomidou.mybatisplus.annotations.TableName
import com.baomidou.mybatisplus.enums.IdType
import org.hibernate.validator.constraints.Range

import java.math.BigInteger
import java.util.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Past
import javax.validation.constraints.Pattern
import java.io.Serializable


/**
 * 文章内容实体类
 *
 * @author ErYang
 */
@TableName("cms_article_content")
class ArticleContent : Model<ArticleContent>(), Serializable {

    companion object {

        private const val serialVersionUID = 6374484025409424089L

        /* 属性名称常量开始 */

        // -------------------------------------------------------------------------------------------------

        /**
         *表名
         */
        const val TABLE_NAME: String = "cms_article_content"

        /**
         *表前缀
         */
        const val TABLE_PREFIX: String = "cms_"

        /**
         * 主键，和文章表共用ID
         */
        const val ID: String = "id"

        /**
         * 文章内容
         */
        const val CONTENT: String = "content"

        /**
         * 文章内容html内容
         */
        const val CONTENT_HTML: String = "content_html"

        /**
         * 创建时间
         */
        const val GMT_CREATE: String = "gmt_create"

        /**
         * 创建IP
         */
        const val GMT_CREATE_IP: String = "gmt_create_ip"

        /**
         * 修改时间
         */
        const val GMT_MODIFIED: String = "gmt_modified"

        /**
         * 状态(1是正常，2是禁用，3是删除)
         */
        const val STATUS: String = "status"

        // -------------------------------------------------------------------------------------------------

        /* 属性名称常量结束 */

        /**
         * 现有的对象赋值给一个新的对象
         *
         * @param articleContent  ArticleContent
         * @return ArticleContent
         */
        fun copy2New(articleContent: ArticleContent): ArticleContent {
            val newArticleContent = ArticleContent()
            newArticleContent.id = articleContent.id
            newArticleContent.content = articleContent.content
            newArticleContent.contentHtml = articleContent.contentHtml
            newArticleContent.gmtCreate = articleContent.gmtCreate
            newArticleContent.gmtCreateIp = articleContent.gmtCreateIp
            newArticleContent.gmtModified = articleContent.gmtModified
            newArticleContent.status = articleContent.status
            return newArticleContent
        }

        // -------------------------------------------------------------------------------------------------

    }


    /**
     * 主键，和文章表共用ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    var id: BigInteger? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 文章内容
     */
    @NotNull(message = "validation.cms.articleContent.content.length")
    var content: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 文章内容html内容
     */
    @NotNull(message = "validation.cms.articleContent.contentHtml.length")
    var contentHtml: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 创建时间
     */
    @Past(message = "validation.gmtCreate.past")
    var gmtCreate: Date? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 创建IP
     */
    @Pattern(regexp = "^[0-9.]{6,50}\$", message = "validation.gmtCreateIp.pattern")
    var gmtCreateIp: String? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 修改时间
     */
    @Past(message = "validation.gmtModified.past")
    var gmtModified: Date? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 状态(1是正常，2是禁用，3是删除)
     */
    @NotNull(message = "validation.status.range")
    @Range(min = 1, max = 3, message = "validation.status.range")
    var status: Int? = null


    // -------------------------------------------------------------------------------------------------

    override fun pkVal(): BigInteger? {
        return this.id
    }

    // -------------------------------------------------------------------------------------------------

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ArticleContent

        if (id != other.id) return false
        if (content != other.content) return false
        if (contentHtml != other.contentHtml) return false
        if (gmtCreate != other.gmtCreate) return false
        if (gmtCreateIp != other.gmtCreateIp) return false
        if (gmtModified != other.gmtModified) return false
        if (status != other.status) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (content?.hashCode() ?: 0)
        result = 31 * result + (contentHtml?.hashCode() ?: 0)
        result = 31 * result + (gmtCreate?.hashCode() ?: 0)
        result = 31 * result + (gmtCreateIp?.hashCode() ?: 0)
        result = 31 * result + (gmtModified?.hashCode() ?: 0)
        result = 31 * result + (status ?: 0)
        return result
    }

    override fun toString(): String {
        return "ArticleContent(id=$id, content=$content, contentHtml=$contentHtml, gmtCreate=$gmtCreate, gmtCreateIp=$gmtCreateIp, gmtModified=$gmtModified, status=$status)"
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ArticleContent class

/* End of file ArticleContent.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/model/entity/cms/ArticleContent.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
