/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.system.impl


import com.baomidou.mybatisplus.mapper.SqlHelper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import wang.encoding.mroot.common.service.BaseServiceImpl
import wang.encoding.mroot.common.util.HibernateValidationUtil
import wang.encoding.mroot.model.entity.system.ScheduleJobLog
import wang.encoding.mroot.model.enums.StatusEnum
import org.springframework.stereotype.Service
import wang.encoding.mroot.mapper.system.ScheduleJobLogMapper
import wang.encoding.mroot.service.system.ScheduleJobLogService

import java.math.BigInteger
import java.time.Instant
import java.util.Date


/**
 * 后台 定时任务记录 Service 接口实现类
 *
 * @author ErYang
 */
@Service
class ScheduleJobLogServiceImpl : BaseServiceImpl
<ScheduleJobLogMapper, ScheduleJobLog>(), ScheduleJobLogService {

    companion object {
        /**
         * logger
         */
        private val logger: Logger = LoggerFactory.getLogger(ScheduleJobLogServiceImpl::class.java)
    }


    /**
     * 初始化新增 ScheduleJobLog 对象
     *
     * @param scheduleJobLog ScheduleJobLog
     * @return ScheduleJobLog
     */
    override fun initSaveScheduleJobLog(scheduleJobLog: ScheduleJobLog): ScheduleJobLog {
        scheduleJobLog.status = StatusEnum.NORMAL.key
        scheduleJobLog.gmtCreate = Date.from(Instant.now())
        return scheduleJobLog
    }

    // -------------------------------------------------------------------------------------------------


    /**
     * Hibernate Validation 验证
     */
    override fun validationScheduleJobLog(scheduleJobLog: ScheduleJobLog): String? {
        return HibernateValidationUtil.validateEntity(scheduleJobLog)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 定时任务记录
     *
     * @param scheduleJobLog ScheduleJobLog
     * @return ID  BigInteger
     */
    override fun saveBackId(scheduleJobLog: ScheduleJobLog): BigInteger? {
        val id: Int? = super.save(scheduleJobLog)
        if (null != id && 0 < id) {
            return scheduleJobLog.id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 定时任务删除
     *
     * @param size Int 数量
     */
    override fun remove2QuartzJob(size: Int): Boolean {
        val params = HashMap<String, Int>()
        params["size"] = size
        return SqlHelper.retBool(superMapper!!.remove2QuartzJob(params))
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ScheduleJobLogServiceImpl class

/* End of file ScheduleJobLogServiceImpl.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/service/system/impl/ScheduleJobLogServiceImpl.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
