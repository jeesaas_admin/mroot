package wang.encoding.mroot.common.util;


import org.apache.commons.lang3.StringUtils;
import wang.encoding.mroot.common.exception.UtilException;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 图片处理工具类
 *
 * @author mutou
 */
public class ImageUtil {


    /**
     * 时间格式
     */
    private final static DateFormat DATETIME_FORMAT = new SimpleDateFormat("yyyyMMddHHmmssSSS");

    /**
     * 点名称
     */
    private final static String DOT = ".";

    /**
     * 图片扩展名
     */
    public static final String[] IMAGE_EXTENSIONS = new String[]{"jpeg", "jpg", "png", "gif", "bmp", "psd"};

    /**
     * 禁止实例化
     */
    private ImageUtil() {

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 不是图片扩展名
     *
     * @param extension 类型
     * @return true（否）/false（是）
     */
    public static Boolean isNotImage(final String extension) {
        return !ImageUtil.isImage(extension);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 是图片扩展名
     *
     * @param extension 类型
     * @return true（是）/false（否）
     */
    public static Boolean isImage(final String extension) {
        if (StringUtil.isBlank(extension)) {
            return false;
        }
        for (String imageExtension : IMAGE_EXTENSIONS) {
            if (StringUtils.equalsIgnoreCase(imageExtension, extension)) {
                return true;
            }
        }
        return false;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 上传图片
     *
     * @param inputStream InputStream
     * @param imagePath   路径
     * @param fileName    名称
     * @return 新图片文件
     */
    public static File uploadImageByInputStream(final InputStream inputStream,
                                                final String imagePath, final String fileName)
            throws UtilException {
        // 重命名
        String name = ImageUtil.getDatetimeFormat() + (Math.random() * 9000 + 1000);
        name = name.replace(DOT, "");
        // 图片类型
        String type = FileUtil.getSuffixByFilename(fileName);
        name = name + DOT + type;

        BufferedInputStream input = new BufferedInputStream(inputStream);
        BufferedOutputStream output;
        try {
            output = new BufferedOutputStream(new FileOutputStream(imagePath + "/" + name));
            int read = input.read();
            while (-1 != read) {
                output.write(read);
                read = input.read();
            }
            output.flush();
            output.close();
        } catch (IOException e) {
            throw new UtilException(e.getMessage());
        }
        return new File(imagePath + "/" + name);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * datetimeFormat 格式日期
     *
     * @return yyyyMMddHHmmssSSS
     */
    private static String getDatetimeFormat() {
        return DATETIME_FORMAT.format(new Date());
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ImageUtil class

/* End of file ImageUtil.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/common/util/ImageUtil.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
