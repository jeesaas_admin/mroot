/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.config

import com.alibaba.fastjson.serializer.SerializerFeature
import com.alibaba.fastjson.support.config.FastJsonConfig
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter
import org.springframework.boot.autoconfigure.http.HttpMessageConverters
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.HttpMessageConverter


/**
 * FastJson配置
 *
 * @author ErYang
 */
@Configuration
@EnableCaching
class FastJsonConfiguration {

    @Bean
    fun fastJsonHttpMessageConverter(): HttpMessageConverters {
        // 定义一个转换消息的对象
        val fastConverter = FastJsonHttpMessageConverter()

        // 添加 fastjson 的配置信息 比如 ：是否要格式化返回的json数据
        val fastJsonConfig = FastJsonConfig()

        fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat)

        // 在转换器中添加配置信息
        fastConverter.fastJsonConfig = fastJsonConfig

        return HttpMessageConverters(fastConverter as HttpMessageConverter<*>)

    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End FastJsonConfiguration class

/* End of file FastJsonConfiguration.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/common/config/FastJsonConfiguration.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
