-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        8.0.11 - MySQL Community Server - GPL
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 导出 mroot 的数据库结构
CREATE DATABASE IF NOT EXISTS `mroot` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `mroot`;

-- 导出  表 mroot.cms_article 结构
CREATE TABLE IF NOT EXISTS `cms_article` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '文章ID',
  `category_id` bigint(20) unsigned NOT NULL COMMENT '文章分类ID',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `description` varchar(255) NOT NULL COMMENT '描述',
  `cover` varchar(255) DEFAULT NULL COMMENT '封面',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `view` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '浏览量',
  `level` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '优先级，数字越大级别越高',
  `link_uri` varchar(255) DEFAULT NULL COMMENT '外链',
  `show` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '是否前台可见，1是前后台都可见；2是后台可见',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '状态(1是正常，2是禁用，3是删除)',
  `gmt_create` datetime(3) NOT NULL COMMENT '创建时间',
  `gmt_create_ip` varchar(64) NOT NULL COMMENT '创建IP',
  `gmt_modified` datetime(3) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='文章表';

-- 正在导出表  mroot.cms_article 的数据：~0 rows (大约)
DELETE FROM `cms_article`;
/*!40000 ALTER TABLE `cms_article` DISABLE KEYS */;
INSERT INTO `cms_article` (`id`, `category_id`, `title`, `description`, `cover`, `sort`, `view`, `level`, `link_uri`, `show`, `status`, `gmt_create`, `gmt_create_ip`, `gmt_modified`) VALUES
	(1, 1, '系统简介', '系统简介', '/assets/img/article/20180619/201806191636506741497191561172811.jpg', 1, 1, 1, NULL, 1, 1, '2018-06-07 10:25:30.860', '192.168.64.1', '2018-06-19 16:37:08.059');
/*!40000 ALTER TABLE `cms_article` ENABLE KEYS */;

-- 导出  表 mroot.cms_article_content 结构
CREATE TABLE IF NOT EXISTS `cms_article_content` (
  `id` bigint(20) unsigned NOT NULL COMMENT '主键，和文章表共用ID',
  `content` text NOT NULL COMMENT '文章内容',
  `content_html` text NOT NULL COMMENT '文章内容html内容',
  `gmt_create` datetime(3) NOT NULL COMMENT '创建时间',
  `gmt_create_ip` varchar(50) NOT NULL COMMENT '创建IP',
  `gmt_modified` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '状态(1是正常，2是禁用，3是删除)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章内容表';

-- 正在导出表  mroot.cms_article_content 的数据：~0 rows (大约)
DELETE FROM `cms_article_content`;
/*!40000 ALTER TABLE `cms_article_content` DISABLE KEYS */;
INSERT INTO `cms_article_content` (`id`, `content`, `content_html`, `gmt_create`, `gmt_create_ip`, `gmt_modified`, `status`) VALUES
	(1, '<pre style="background-color:#2b2b2b;color:#a9b7c6;font-family:\'宋体\';font-size:13.5pt;">MRoot项目简介\r\n\r\nMRoot采用Spring&nbsp;Boot2整合其他开源框架架构，使用Kotlin语言开发的通用后台管理系统（100%兼容Java,可以与Java互操作）\r\n\r\n代码生成器，可以生成业务90%的代码，可快速完成某个业务的开发\r\n\r\n使用Maven对项目进行模块化管理，提高项目的易开发性、扩展性\r\n\r\n\r\n具有如下特点\r\n\r\n灵活的权限控制，可控制权限到按钮级别\r\n\r\n完善的角色管理及数据权限\r\n\r\n完善的XSS防范及脚本过滤，彻底杜绝XSS攻击\r\n\r\n友好的代码结构及注释，便于阅读及二次开发\r\n\r\nQuartz定时任务，可动态完成任务的添加、修改、删除、暂停、恢复及日志查看等功能\r\n\r\n前端使用Bootstrap，优美的页面，丰富的插件\r\n\r\n主要功能\r\n\r\n数据库：Druid数据库连接池，监控数据库访问性能，统计SQL的执行性能\r\n\r\n持久层：MyBatis持久化，使用MyBatis-Plus优化，减少sql开发量，使用Hibernate&nbsp;Validation进行数据验证\r\n\r\nMVC：基于Spring&nbsp;Mvc注解，Rest风格Controller，Exception统一管理\r\n\r\n任务调度：Spring+Quartz,&nbsp;可动态完成任务的添加、修改、删除、暂停、恢复及日志查看等功能\r\n\r\n基于Spring的国际化信息\r\n\r\nShiro进行权限控制\r\n\r\n缓存：注解缓存数据\r\n\r\n自定义线程池、异步任务\r\n\r\n日志：logback打印日志，存入数据库，同时基于时间和文件大小分割日志文件\r\n\r\n工具类：加密解密、字符串处理等等\r\n\r\n\r\n技术选型\r\n\r\n开发语言：Kotlin\r\n\r\n核心框架：Spring&nbsp;Boot2\r\n\r\n数据库连接池：Alibaba&nbsp;Druid\r\n\r\n持久层框架：MyBatis&nbsp;+&nbsp;MyBatis-Plus\r\n\r\n安全框架：Apache&nbsp;Shiro\r\n\r\n任务调度：Spring&nbsp;+&nbsp;Quartz\r\n\r\n缓存框架：Ehcache3\r\n\r\n日志管理：SLF4J、Logback\r\n\r\n验证框架：Hibernate&nbsp;Validation\r\n\r\n模板：Freemarker\r\n\r\n前端框架：Bootstrap\r\n\r\n开发环境\r\n\r\nJDK10+、Maven3.5.2+、Mysql8+、Spring&nbsp;Boot2\r\n\r\n本地部署\r\n\r\n通过git下载源码\r\n\r\n创建数据库mroot，数据库编码为UTF-8\r\n\r\n执行db/mroot.sql文件】\r\n\r\n修改application-XXX.properties文件，更新MySQL账号和密码\r\n\r\nIDEA运行MRootAdminApplication.kt，则可启动项目\r\n\r\nmroot-admin访问路径：你的IP地址:端口/login\r\n\r\n账号密码：admin/123456\r\n\r\n\r\n\r\n已完成功能\r\n\r\n\r\n用户管理：配置用户角色\r\n\r\n角色管理：配置角色所拥有的权限\r\n\r\n菜单管理：配置角色所拥有的权限\r\n\r\n操作日志：系统操作日志记录和查询\r\n\r\n代码生成：可生成90%的业务代码\r\n\r\n缓存管理：Ehcache3\r\n\r\n文章管理：百度编辑器的上传处理</pre><p><br></p>', '<pre style="background-color:#2b2b2b;color:#a9b7c6;font-family:\'宋体\';font-size:13.5pt;">MRoot项目简介\r\n\r\nMRoot采用Spring&nbsp;Boot2整合其他开源框架架构，使用Kotlin语言开发的通用后台管理系统（100%兼容Java,可以与Java互操作）\r\n\r\n代码生成器，可以生成业务90%的代码，可快速完成某个业务的开发\r\n\r\n使用Maven对项目进行模块化管理，提高项目的易开发性、扩展性\r\n\r\n\r\n具有如下特点\r\n\r\n灵活的权限控制，可控制权限到按钮级别\r\n\r\n完善的角色管理及数据权限\r\n\r\n完善的XSS防范及脚本过滤，彻底杜绝XSS攻击\r\n\r\n友好的代码结构及注释，便于阅读及二次开发\r\n\r\nQuartz定时任务，可动态完成任务的添加、修改、删除、暂停、恢复及日志查看等功能\r\n\r\n前端使用Bootstrap，优美的页面，丰富的插件\r\n\r\n主要功能\r\n\r\n数据库：Druid数据库连接池，监控数据库访问性能，统计SQL的执行性能\r\n\r\n持久层：MyBatis持久化，使用MyBatis-Plus优化，减少sql开发量，使用Hibernate&nbsp;Validation进行数据验证\r\n\r\nMVC：基于Spring&nbsp;Mvc注解，Rest风格Controller，Exception统一管理\r\n\r\n任务调度：Spring+Quartz,&nbsp;可动态完成任务的添加、修改、删除、暂停、恢复及日志查看等功能\r\n\r\n基于Spring的国际化信息\r\n\r\nShiro进行权限控制\r\n\r\n缓存：注解缓存数据\r\n\r\n自定义线程池、异步任务\r\n\r\n日志：logback打印日志，存入数据库，同时基于时间和文件大小分割日志文件\r\n\r\n工具类：加密解密、字符串处理等等\r\n\r\n\r\n技术选型\r\n\r\n开发语言：Kotlin\r\n\r\n核心框架：Spring&nbsp;Boot2\r\n\r\n数据库连接池：Alibaba&nbsp;Druid\r\n\r\n持久层框架：MyBatis&nbsp;+&nbsp;MyBatis-Plus\r\n\r\n安全框架：Apache&nbsp;Shiro\r\n\r\n任务调度：Spring&nbsp;+&nbsp;Quartz\r\n\r\n缓存框架：Ehcache3\r\n\r\n日志管理：SLF4J、Logback\r\n\r\n验证框架：Hibernate&nbsp;Validation\r\n\r\n模板：Freemarker\r\n\r\n前端框架：Bootstrap\r\n\r\n开发环境\r\n\r\nJDK10+、Maven3.5.2+、Mysql8+、Spring&nbsp;Boot2\r\n\r\n本地部署\r\n\r\n通过git下载源码\r\n\r\n创建数据库mroot，数据库编码为UTF-8\r\n\r\n执行db/mroot.sql文件】\r\n\r\n修改application-XXX.properties文件，更新MySQL账号和密码\r\n\r\nIDEA运行MRootAdminApplication.kt，则可启动项目\r\n\r\nmroot-admin访问路径：你的IP地址:端口/login\r\n\r\n账号密码：admin/123456\r\n\r\n\r\n\r\n已完成功能\r\n\r\n\r\n用户管理：配置用户角色\r\n\r\n角色管理：配置角色所拥有的权限\r\n\r\n菜单管理：配置角色所拥有的权限\r\n\r\n操作日志：系统操作日志记录和查询\r\n\r\n代码生成：可生成90%的业务代码\r\n\r\n缓存管理：Ehcache3\r\n\r\n文章管理：百度编辑器的上传处理</pre><p><br></p>', '2018-06-07 10:25:31.417', '192.168.64.1', '2018-06-19 16:36:52.738', 1);
/*!40000 ALTER TABLE `cms_article_content` ENABLE KEYS */;

-- 导出  表 mroot.cms_category 结构
CREATE TABLE IF NOT EXISTS `cms_category` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `pid` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '父级分类',
  `type` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '1主分类2次分类3分类',
  `name` varchar(80) NOT NULL COMMENT '标识',
  `title` varchar(80) NOT NULL COMMENT '名称',
  `audit` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '发布的文章是否需要审核，1是需要；2是不需要',
  `allow` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '是否允许发布内容，1允许；2不允许',
  `show` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '是否前台可见，1是前后台都可见；2是后台可见',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '状态(1是正常，2是禁用，3是删除)',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `gmt_create` datetime(3) NOT NULL COMMENT '创建时间',
  `gmt_create_ip` varchar(64) NOT NULL COMMENT '创建IP',
  `gmt_modified` datetime(3) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  UNIQUE KEY `uk_title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='文章分类表';

-- 正在导出表  mroot.cms_category 的数据：~0 rows (大约)
DELETE FROM `cms_category`;
/*!40000 ALTER TABLE `cms_category` DISABLE KEYS */;
INSERT INTO `cms_category` (`id`, `pid`, `type`, `name`, `title`, `audit`, `allow`, `show`, `sort`, `status`, `remark`, `gmt_create`, `gmt_create_ip`, `gmt_modified`) VALUES
	(1, 0, 1, 'presentation', '系统介绍', 2, 1, 1, 1, 1, '系统介绍', '2018-06-01 15:52:50.903', '127.0.0.1', '2018-06-09 12:16:53.578');
/*!40000 ALTER TABLE `cms_category` ENABLE KEYS */;

-- 导出  表 mroot.logging_event 结构
CREATE TABLE IF NOT EXISTS `logging_event` (
  `event_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志ID',
  `level_string` varchar(254) NOT NULL COMMENT '类型',
  `caller_filename` varchar(254) NOT NULL COMMENT '文件名称',
  `logger_name` varchar(254) NOT NULL COMMENT '日志名称',
  `thread_name` varchar(254) DEFAULT NULL COMMENT '线程名称',
  `caller_class` varchar(254) NOT NULL COMMENT '类名',
  `caller_method` varchar(254) NOT NULL COMMENT '方法',
  `arg0` varchar(254) DEFAULT NULL COMMENT '参数',
  `arg1` varchar(254) DEFAULT NULL COMMENT '参数',
  `arg2` varchar(254) DEFAULT NULL COMMENT '参数',
  `arg3` varchar(254) DEFAULT NULL COMMENT '参数',
  `formatted_message` text NOT NULL COMMENT '日志信息',
  `reference_flag` smallint(6) DEFAULT NULL COMMENT '引用',
  `caller_line` char(4) NOT NULL COMMENT '代码行',
  `timestmp` bigint(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='logback日志表';

-- 正在导出表  mroot.logging_event 的数据：~528 rows (大约)
DELETE FROM `logging_event`;
/*!40000 ALTER TABLE `logging_event` DISABLE KEYS */;
INSERT INTO `logging_event` (`event_id`, `level_string`, `caller_filename`, `logger_name`, `thread_name`, `caller_class`, `caller_method`, `arg0`, `arg1`, `arg2`, `arg3`, `formatted_message`, `reference_flag`, `caller_line`, `timestmp`) VALUES
	(1, 'INFO', 'Jdk14Logger.java', 'wang.encoding.mroot.admin.MRootAdminApplication', 'RMI TCP Connection(3)-127.0.0.1', 'org.apache.commons.logging.impl.Jdk14Logger', 'log', NULL, NULL, NULL, NULL, 'Starting MRootAdminApplication v1.0.0 on DESKTOP-B5SGLV5 with PID 12088 (E:\\git\\finance\\mroot-admin\\target\\admin\\WEB-INF\\classes started by mutou in E:\\server\\apache-tomcat-9.0.7\\bin)', 0, '87', 1529394034041),
	(2, 'INFO', 'Jdk14Logger.java', 'wang.encoding.mroot.admin.MRootAdminApplication', 'RMI TCP Connection(3)-127.0.0.1', 'org.apache.commons.logging.impl.Jdk14Logger', 'log', NULL, NULL, NULL, NULL, 'The following profiles are active: dev', 0, '87', 1529394034101),
	(3, 'INFO', 'SystemConfigListener.kt', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'main', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'contextDestroyed', NULL, NULL, NULL, NULL, '>>>>>>>>ServletContext销毁监听器<<<<<<<<', 0, '150', 1529394087313),
	(4, 'INFO', 'Jdk14Logger.java', 'wang.encoding.mroot.admin.MRootAdminApplication', 'RMI TCP Connection(3)-127.0.0.1', 'org.apache.commons.logging.impl.Jdk14Logger', 'log', NULL, NULL, NULL, NULL, 'Starting MRootAdminApplication v1.0.0 on DESKTOP-B5SGLV5 with PID 10936 (E:\\git\\finance\\mroot-admin\\target\\admin\\WEB-INF\\classes started by mutou in E:\\server\\apache-tomcat-9.0.7\\bin)', 0, '87', 1529394326557),
	(5, 'INFO', 'Jdk14Logger.java', 'wang.encoding.mroot.admin.MRootAdminApplication', 'RMI TCP Connection(3)-127.0.0.1', 'org.apache.commons.logging.impl.Jdk14Logger', 'log', NULL, NULL, NULL, NULL, 'The following profiles are active: dev', 0, '87', 1529394326778),
	(6, 'INFO', 'Jdk14Logger.java', 'wang.encoding.mroot.admin.MRootAdminApplication', 'RMI TCP Connection(3)-127.0.0.1', 'org.apache.commons.logging.impl.Jdk14Logger', 'log', NULL, NULL, NULL, NULL, 'Started MRootAdminApplication in 30.726 seconds (JVM running for 42.609)', 0, '87', 1529394355631),
	(7, 'INFO', 'SystemConfigListener.kt', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'RMI TCP Connection(3)-127.0.0.1', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'contextInitialized', NULL, NULL, NULL, NULL, '>>>>>>>>ServletContext启动监听器<<<<<<<<', 0, '94', 1529394355749),
	(8, 'INFO', 'SystemConfigListener.kt', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'RMI TCP Connection(3)-127.0.0.1', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'contextInitialized', NULL, NULL, NULL, NULL, '>>>>>>>>设置PROFILE[开发环境]<<<<<<<<', 0, '101', 1529394355789),
	(9, 'INFO', 'SystemConfigListener.kt', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'RMI TCP Connection(3)-127.0.0.1', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'contextInitialized', NULL, NULL, NULL, NULL, '>>>>>>>>CONTEXT_PATH[]<<<<<<<<', 0, '106', 1529394355830),
	(10, 'INFO', 'SystemConfigListener.kt', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'RMI TCP Connection(3)-127.0.0.1', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'contextInitialized', NULL, NULL, NULL, NULL, '>>>>>>>>设置SERVER_PORT[9001]<<<<<<<<', 0, '117', 1529394355843),
	(11, 'INFO', 'SystemConfigListener.kt', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'RMI TCP Connection(3)-127.0.0.1', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'contextInitialized', NULL, NULL, NULL, NULL, '>>>>>>>>设置资源信息集合[key=GLOBAL_RESOURCE_MAP,map.size=4]<<<<<<<<', 0, '123', 1529394355855),
	(12, 'INFO', 'SystemConfigListener.kt', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'RMI TCP Connection(3)-127.0.0.1', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'contextInitialized', NULL, NULL, NULL, NULL, '>>>>>>>>设置数据库配置信息Global[CONFIG_MAP]<<<<<<<<', 0, '131', 1529394355878),
	(13, 'ERROR', 'QuartzScheduleUtil.kt', 'wang.encoding.mroot.admin.common.quartz.QuartzScheduleUtil', 'RMI TCP Connection(3)-127.0.0.1', 'wang.encoding.mroot.admin.common.quartz.QuartzScheduleUtil', 'editScheduleJob', NULL, NULL, NULL, NULL, '>>>>>>>>更新定时任务失败org.quartz.JobPersistenceException: Couldn\'t remove trigger: wang.encoding.finance.admin.common.quartz.QuartzScheduleJob [See nested exception: java.lang.ClassNotFoundException: wang.encoding.finance.admin.common.quartz.QuartzScheduleJob]<<<<<<<<', 0, '132', 1529394356206),
	(14, 'INFO', 'SystemConfigListener.kt', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'main', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'contextDestroyed', NULL, NULL, NULL, NULL, '>>>>>>>>ServletContext销毁监听器<<<<<<<<', 0, '150', 1529397744693);
/*!40000 ALTER TABLE `logging_event` ENABLE KEYS */;

-- 导出  表 mroot.logging_event_exception 结构
CREATE TABLE IF NOT EXISTS `logging_event_exception` (
  `event_id` bigint(20) NOT NULL COMMENT '日志ID',
  `i` smallint(6) NOT NULL COMMENT '类型',
  `trace_line` varchar(254) NOT NULL COMMENT '异常',
  PRIMARY KEY (`event_id`,`i`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='logback日志异常详情表';

-- 正在导出表  mroot.logging_event_exception 的数据：~0 rows (大约)
DELETE FROM `logging_event_exception`;
/*!40000 ALTER TABLE `logging_event_exception` DISABLE KEYS */;
/*!40000 ALTER TABLE `logging_event_exception` ENABLE KEYS */;

-- 导出  表 mroot.logging_event_property 结构
CREATE TABLE IF NOT EXISTS `logging_event_property` (
  `event_id` bigint(20) NOT NULL COMMENT '日志ID',
  `mapped_key` varchar(254) NOT NULL COMMENT '设置key',
  `mapped_value` text COMMENT '设置值',
  PRIMARY KEY (`event_id`,`mapped_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='logback属性表';

-- 正在导出表  mroot.logging_event_property 的数据：~0 rows (大约)
DELETE FROM `logging_event_property`;
/*!40000 ALTER TABLE `logging_event_property` DISABLE KEYS */;
/*!40000 ALTER TABLE `logging_event_property` ENABLE KEYS */;

-- 导出  表 mroot.qrtz_blob_triggers 结构
CREATE TABLE IF NOT EXISTS `qrtz_blob_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  mroot.qrtz_blob_triggers 的数据：~0 rows (大约)
DELETE FROM `qrtz_blob_triggers`;
/*!40000 ALTER TABLE `qrtz_blob_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `qrtz_blob_triggers` ENABLE KEYS */;

-- 导出  表 mroot.qrtz_calendars 结构
CREATE TABLE IF NOT EXISTS `qrtz_calendars` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  mroot.qrtz_calendars 的数据：~0 rows (大约)
DELETE FROM `qrtz_calendars`;
/*!40000 ALTER TABLE `qrtz_calendars` DISABLE KEYS */;
/*!40000 ALTER TABLE `qrtz_calendars` ENABLE KEYS */;

-- 导出  表 mroot.qrtz_cron_triggers 结构
CREATE TABLE IF NOT EXISTS `qrtz_cron_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(120) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  mroot.qrtz_cron_triggers 的数据：~0 rows (大约)
DELETE FROM `qrtz_cron_triggers`;
/*!40000 ALTER TABLE `qrtz_cron_triggers` DISABLE KEYS */;
INSERT INTO `qrtz_cron_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`, `CRON_EXPRESSION`, `TIME_ZONE_ID`) VALUES
	('quartzScheduler', 'MROOT_TRIGGER', 'MROOT_GROUP_TRIGGER', '0 0 0/1 * * ? *', 'Asia/Shanghai');
/*!40000 ALTER TABLE `qrtz_cron_triggers` ENABLE KEYS */;

-- 导出  表 mroot.qrtz_fired_triggers 结构
CREATE TABLE IF NOT EXISTS `qrtz_fired_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`),
  KEY `IDX_QRTZ_FT_TRIG_INST_NAME` (`SCHED_NAME`,`INSTANCE_NAME`),
  KEY `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY` (`SCHED_NAME`,`INSTANCE_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_FT_J_G` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_T_G` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_FT_TG` (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  mroot.qrtz_fired_triggers 的数据：~0 rows (大约)
DELETE FROM `qrtz_fired_triggers`;
/*!40000 ALTER TABLE `qrtz_fired_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `qrtz_fired_triggers` ENABLE KEYS */;

-- 导出  表 mroot.qrtz_job_details 结构
CREATE TABLE IF NOT EXISTS `qrtz_job_details` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_J_REQ_RECOVERY` (`SCHED_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_J_GRP` (`SCHED_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  mroot.qrtz_job_details 的数据：~0 rows (大约)
DELETE FROM `qrtz_job_details`;
/*!40000 ALTER TABLE `qrtz_job_details` DISABLE KEYS */;
INSERT INTO `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`, `DESCRIPTION`, `JOB_CLASS_NAME`, `IS_DURABLE`, `IS_NONCONCURRENT`, `IS_UPDATE_DATA`, `REQUESTS_RECOVERY`, `JOB_DATA`) VALUES
	('quartzScheduler', 'MROOT_JOB1', 'MROOT_GROUP_JOB', NULL, 'wang.encoding.finance.admin.common.quartz.QuartzScheduleJob', '0', '0', '0', '0', _binary 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61703FE8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D6170133F3F760A3F00025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC13F603F000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000B7363686564756C654A6F627372003577616E672E656E636F64696E672E66696E616E63652E6D6F64656C2E656E746974792E73797374656D2E5363686564756C654A6F62D8F80EC74AAEF57B02000D4C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C0009676D744372656174657400104C6A6176612F7574696C2F446174653B4C000B676D74437265617465497071007E00094C000B676D744D6F64696669656471007E000A4C000269647400164C6A6176612F6D6174682F426967496E74656765723B4C000A6D6574686F644E616D6571007E00094C00046E616D6571007E00094C0006706172616D7371007E00094C000672656D61726B71007E00094C0004736F72747400104C6A6176612F6C616E672F4C6F6E673B4C00067374617475737400134C6A6176612F6C616E672F496E74656765723B4C00057469746C6571007E00097872002B636F6D2E62616F6D69646F752E6D796261746973706C75732E6163746976657265636F72642E4D6F64656C0000000000000001020000787074000D726571756573744C6F674A6F6274000F30203020302F31202A202A203F202A7372000E6A6176612E7574696C2E44617465686A3F4B59741903000078707708000001636CB77A90787400093132372E302E302E317371007E0012770800000163F355445578737200146A6176612E6D6174682E426967496E74656765728CFC3F3F3F030006490008626974436F756E744900096269744C656E67746849001366697273744E6F6E7A65726F427974654E756D49000C6C6F776573745365744269744900067369676E756D5B00096D61676E69747564657400025B42787200106A6176612E6C616E672E4E756D62657286AC3F0B94E03F00007870FFFFFFFFFFFFFFFFFFFFFF3FFFFF3F00000001757200025B42ACF3173F08543F0000787000000001017874000B72656D6F766553697A65327400056A6F62303170740022E4B880E6ACA1E588A0E9993FE69DA1E5908EE58FB0E8AFB7E6B182E8AEB0E5BD957372000E6A6176612E6C616E672E4C6F6E673B8BE490CC3F3F00014A000576616C75657871007E00180000000000000001737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E001800000001740018E588A0E999A4E5908EE58FB0E8AFB7E6B182E8AEB0E5BD957800);
/*!40000 ALTER TABLE `qrtz_job_details` ENABLE KEYS */;

-- 导出  表 mroot.qrtz_locks 结构
CREATE TABLE IF NOT EXISTS `qrtz_locks` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  mroot.qrtz_locks 的数据：~2 rows (大约)
DELETE FROM `qrtz_locks`;
/*!40000 ALTER TABLE `qrtz_locks` DISABLE KEYS */;
INSERT INTO `qrtz_locks` (`SCHED_NAME`, `LOCK_NAME`) VALUES
	('quartzScheduler', 'STATE_ACCESS'),
	('quartzScheduler', 'TRIGGER_ACCESS');
/*!40000 ALTER TABLE `qrtz_locks` ENABLE KEYS */;

-- 导出  表 mroot.qrtz_paused_trigger_grps 结构
CREATE TABLE IF NOT EXISTS `qrtz_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  mroot.qrtz_paused_trigger_grps 的数据：~0 rows (大约)
DELETE FROM `qrtz_paused_trigger_grps`;
/*!40000 ALTER TABLE `qrtz_paused_trigger_grps` DISABLE KEYS */;
/*!40000 ALTER TABLE `qrtz_paused_trigger_grps` ENABLE KEYS */;

-- 导出  表 mroot.qrtz_scheduler_state 结构
CREATE TABLE IF NOT EXISTS `qrtz_scheduler_state` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  mroot.qrtz_scheduler_state 的数据：~0 rows (大约)
DELETE FROM `qrtz_scheduler_state`;
/*!40000 ALTER TABLE `qrtz_scheduler_state` DISABLE KEYS */;
INSERT INTO `qrtz_scheduler_state` (`SCHED_NAME`, `INSTANCE_NAME`, `LAST_CHECKIN_TIME`, `CHECKIN_INTERVAL`) VALUES
	('quartzScheduler', 'DESKTOP-B5SGLV51529394346519', 1529397735888, 20000);
/*!40000 ALTER TABLE `qrtz_scheduler_state` ENABLE KEYS */;

-- 导出  表 mroot.qrtz_simple_triggers 结构
CREATE TABLE IF NOT EXISTS `qrtz_simple_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  mroot.qrtz_simple_triggers 的数据：~0 rows (大约)
DELETE FROM `qrtz_simple_triggers`;
/*!40000 ALTER TABLE `qrtz_simple_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `qrtz_simple_triggers` ENABLE KEYS */;

-- 导出  表 mroot.qrtz_simprop_triggers 结构
CREATE TABLE IF NOT EXISTS `qrtz_simprop_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  mroot.qrtz_simprop_triggers 的数据：~0 rows (大约)
DELETE FROM `qrtz_simprop_triggers`;
/*!40000 ALTER TABLE `qrtz_simprop_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `qrtz_simprop_triggers` ENABLE KEYS */;

-- 导出  表 mroot.qrtz_triggers 结构
CREATE TABLE IF NOT EXISTS `qrtz_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_J` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_C` (`SCHED_NAME`,`CALENDAR_NAME`),
  KEY `IDX_QRTZ_T_G` (`SCHED_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_STATE` (`SCHED_NAME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_STATE` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_G_STATE` (`SCHED_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NEXT_FIRE_TIME` (`SCHED_NAME`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST` (`SCHED_NAME`,`TRIGGER_STATE`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  mroot.qrtz_triggers 的数据：~0 rows (大约)
DELETE FROM `qrtz_triggers`;
/*!40000 ALTER TABLE `qrtz_triggers` DISABLE KEYS */;
INSERT INTO `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`, `JOB_NAME`, `JOB_GROUP`, `DESCRIPTION`, `NEXT_FIRE_TIME`, `PREV_FIRE_TIME`, `PRIORITY`, `TRIGGER_STATE`, `TRIGGER_TYPE`, `START_TIME`, `END_TIME`, `CALENDAR_NAME`, `MISFIRE_INSTR`, `JOB_DATA`) VALUES
	('quartzScheduler', 'MROOT_TRIGGER', 'MROOT_GROUP_TRIGGER', 'MROOT_JOB1', 'MROOT_GROUP_JOB', NULL, 1529391600000, -1, 5, 'WAITING', 'CRON', 1529388169000, 0, NULL, 0, _binary 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61703FE8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D6170133F3F760A3F00025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC13F603F000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000B7363686564756C654A6F627372003377616E672E656E636F64696E672E6D726F6F742E6D6F64656C2E656E746974792E73797374656D2E5363686564756C654A6F62D8F80EC74AAEF57B02000D4C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C0009676D744372656174657400104C6A6176612F7574696C2F446174653B4C000B676D74437265617465497071007E00094C000B676D744D6F64696669656471007E000A4C000269647400164C6A6176612F6D6174682F426967496E74656765723B4C000A6D6574686F644E616D6571007E00094C00046E616D6571007E00094C0006706172616D7371007E00094C000672656D61726B71007E00094C0004736F72747400104C6A6176612F6C616E672F4C6F6E673B4C00067374617475737400134C6A6176612F6C616E672F496E74656765723B4C00057469746C6571007E00097872002B636F6D2E62616F6D69646F752E6D796261746973706C75732E6163746976657265636F72642E4D6F64656C0000000000000001020000787074000D726571756573744C6F674A6F6274000F30203020302F31202A202A203F202A7372000E6A6176612E7574696C2E44617465686A3F4B59741903000078707708000001636CB77A90787400093132372E302E302E317371007E0012770800000163F355445578737200146A6176612E6D6174682E426967496E74656765728CFC3F3F3F030006490008626974436F756E744900096269744C656E67746849001366697273744E6F6E7A65726F427974654E756D49000C6C6F776573745365744269744900067369676E756D5B00096D61676E69747564657400025B42787200106A6176612E6C616E672E4E756D62657286AC3F0B94E03F00007870FFFFFFFFFFFFFFFFFFFFFF3FFFFF3F00000001757200025B42ACF3173F08543F0000787000000001017874000B72656D6F766553697A65327400056A6F62303170740022E4B880E6ACA1E588A0E9993FE69DA1E5908EE58FB0E8AFB7E6B182E8AEB0E5BD957372000E6A6176612E6C616E672E4C6F6E673B8BE490CC3F3F00014A000576616C75657871007E00180000000000000001737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E001800000001740018E588A0E999A4E5908EE58FB0E8AFB7E6B182E8AEB0E5BD957800);
/*!40000 ALTER TABLE `qrtz_triggers` ENABLE KEYS */;

-- 导出  表 mroot.system_config 结构
CREATE TABLE IF NOT EXISTS `system_config` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` tinyint(4) unsigned NOT NULL COMMENT '类型(1是内容,2是表达式)',
  `name` varchar(80) NOT NULL COMMENT '标识',
  `title` varchar(80) NOT NULL COMMENT '名称',
  `value` varchar(255) NOT NULL COMMENT '内容',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '状态(1是正常，2是禁用，3是删除)',
  `gmt_create` datetime(3) NOT NULL COMMENT '创建时间',
  `gmt_create_ip` varchar(64) NOT NULL COMMENT '创建IP',
  `gmt_modified` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  UNIQUE KEY `uk_title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='系统配置表';

-- 正在导出表  mroot.system_config 的数据：~3 rows (大约)
DELETE FROM `system_config`;
/*!40000 ALTER TABLE `system_config` DISABLE KEYS */;
INSERT INTO `system_config` (`id`, `type`, `name`, `title`, `value`, `status`, `gmt_create`, `gmt_create_ip`, `gmt_modified`, `sort`, `remark`) VALUES
	(1, 1, 'ADMIN_PAGE_SIZE', '后台分页条数', '15', 1, '2018-04-02 18:01:37.146', '127.0.0.1', '2018-04-24 11:54:38.756', 1, '后台分页条数'),
	(2, 1, 'TEST', '测试', 'fasdfasdf', 3, '2018-04-07 23:30:52.808', '127.0.0.1', '2018-04-08 00:19:38.073', 3, '测试');
/*!40000 ALTER TABLE `system_config` ENABLE KEYS */;

-- 导出  表 mroot.system_request_log 结构
CREATE TABLE IF NOT EXISTS `system_request_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `module` varchar(50) NOT NULL COMMENT '模块',
  `user_id` bigint(20) unsigned DEFAULT NULL COMMENT '用户ID',
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `user_type` varchar(50) DEFAULT NULL COMMENT '用户类型',
  `user_agent` varchar(512) NOT NULL COMMENT '客户端',
  `title` varchar(100) NOT NULL COMMENT '描述',
  `class_name` varchar(100) NOT NULL COMMENT '类名称',
  `method_name` varchar(100) NOT NULL COMMENT '方法名称',
  `session_name` varchar(255) NOT NULL COMMENT 'session名称',
  `url` varchar(255) NOT NULL COMMENT '请求地址',
  `method_type` varchar(20) NOT NULL COMMENT '请求的方法类型',
  `params` varchar(2000) DEFAULT NULL COMMENT '请求参数',
  `result` varchar(1000) DEFAULT NULL COMMENT '返回结果',
  `execute_time` int(11) unsigned NOT NULL COMMENT '执行时间,单位:毫秒',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `gmt_create` datetime(3) NOT NULL COMMENT '创建时间',
  `gmt_create_ip` varchar(50) NOT NULL COMMENT '创建IP',
  `gmt_modified` datetime(3) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COMMENT='请求日志表';

-- 正在导出表  mroot.system_request_log 的数据：~598 rows (大约)
DELETE FROM `system_request_log`;
/*!40000 ALTER TABLE `system_request_log` DISABLE KEYS */;
INSERT INTO `system_request_log` (`id`, `module`, `user_id`, `username`, `user_type`, `user_agent`, `title`, `class_name`, `method_name`, `session_name`, `url`, `method_type`, `params`, `result`, `execute_time`, `remark`, `gmt_create`, `gmt_create_ip`, `gmt_modified`) VALUES
	(1, '后台管理', NULL, NULL, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '用户登录页面', 'wang.encoding.mroot.admin.controller.login.LoginController', 'index', '1d7caaf3-9ebb-491c-a3df-748511e7f98d', 'http://127.0.0.1:9901/login;JSESSIONID=1d7caaf3-9ebb-491c-a3df-748511e7f98d', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/login/login2Dev\'; model is {isVerifyCode=hide}', 65, NULL, '2018-06-19 15:46:02.826', '192.168.64.1', NULL),
	(2, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '处理用户登录', 'wang.encoding.mroot.admin.controller.login.LoginController', 'login', '1d7caaf3-9ebb-491c-a3df-748511e7f98d', 'http://127.0.0.1:9901/doLogin', 'POST', '{username=[admin]},{password=[123456]},{formToken=[d23b5d7b-05aa-4617-91ac-2efbdc5f8da8]},{isVerifyCode=[hide]},{verifyCode=[]},{0=[u]},{1=[s]},{2=[e]},{3=[r]},{4=[n]},{5=[a]},{6=[m]},{7=[e]},{8=[=]},{9=[a]},{10=[d]},{11=[m]},{12=[i]},{13=[n]},{14=[&]},{15=[p]},{16=[a]},{17=[s]},{18=[s]},{19=[w]},{20=[o]},{21=[r]},{22=[d]},{23=[=]},{24=[1]},{25=[2]},{26=[3]},{27=[4]},{28=[5]},{29=[6]},{30=[&]},{31=[f]},{32=[o]},{33=[r]},{34=[m]},{35=[T]},{36=[o]},{37=[k]},{38=[e]},{39=[n]},{40=[=]},{41=[d]},{42=[2]},{43=[3]},{44=[b]},{45=[5]},{46=[d]},{47=[7]},{48=[b]},{49=[-]},{50=[0]},{51=[5]},{52=[a]},{53=[a]},{54=[-]},{55=[4]},{56=[6]},{57=[1]},{58=[7]},{59=[-]},{60=[9]},{61=[1]},{62=[a]},{63=[c]},{64=[-]},{65=[2]},{66=[e]},{67=[f]},{68=[b]},{69=[d]},{70=[c]},{71=[5]},{72=[f]},{73=[8]},{74=[d]},{75=[a]},{76=[8]},{77=[&]},{78=[i]},{79=[s]},{80=[V]},{81=[e]},{82=[r]},{83=[i]},{84=[f]},{85=[y]},{86=[C]},{87=[o]},{88=[d]},{89=[e]},{90=[=]},{91=[h]},{92=[i]},{93=[d]},{94=[e]},{95=[&]},{96=[v]},{97=[e]},{98=[r]},{99=[i]},{100=[f]},{101=[y]},{102=[C]},{103=[o]},{104=[d]},{105=[e]},{106=[=]}', '{"message":"登录成功","url":"/index","state":"ok"}', 335, NULL, '2018-06-19 15:46:19.598', '192.168.64.1', NULL),
	(3, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '主页', 'wang.encoding.mroot.admin.controller.index.IndexController', 'index', '1d7caaf3-9ebb-491c-a3df-748511e7f98d', 'http://127.0.0.1:9901/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/index/index\'; model is {osInfoMap={操作系统信息=Windows 10, 操作版本=10.0, 架构=amd64, Java的运行环境版本=10.0.1, Java运行时环境规范版本=10, Java的虚拟机规范版本=10, Java的类格式版本号=54.0}}', 107, NULL, '2018-06-19 15:46:20.504', '192.168.64.1', NULL),
	(4, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '角色列表', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'index', '1d7caaf3-9ebb-491c-a3df-748511e7f98d', 'http://127.0.0.1:9901/role/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/role/index\'; model is {page= Page:{ [Pagination { total=3 ,size=15 ,pages=1 ,current=1 }], records-size:3 }}', 1063, NULL, '2018-06-19 15:46:34.921', '192.168.64.1', NULL),
	(5, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '角色授权', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'authorization', '1d7caaf3-9ebb-491c-a3df-748511e7f98d', 'http://127.0.0.1:9901/role/authorization/1', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/role/authorization\'; model is {ruleString=2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118, role=Role(id=1, name=admin, title=超级管理员, status=1, gmtCreate=Sat Feb 24 16:03:36 CST 2018, gmtCreateIp=127.0.0.1, gmtModified=null, sort=1, remark=超级管理员拥有最高权限)}', 352, NULL, '2018-06-19 15:46:45.064', '192.168.64.1', NULL),
	(6, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '角色授权', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'authorization', '1d7caaf3-9ebb-491c-a3df-748511e7f98d', 'http://127.0.0.1:9901/role/authorization/1', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/role/authorization\'; model is {ruleString=2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118, role=Role(id=1, name=admin, title=超级管理员, status=1, gmtCreate=Sat Feb 24 16:03:36 CST 2018, gmtCreateIp=127.0.0.1, gmtModified=null, sort=1, remark=超级管理员拥有最高权限)}', 311, NULL, '2018-06-19 15:48:32.521', '192.168.64.1', NULL),
	(7, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '代码生成列表', 'wang.encoding.mroot.admin.controller.system.generate.GenerateCodeController', 'index', '1d7caaf3-9ebb-491c-a3df-748511e7f98d', 'http://127.0.0.1:9901/generateCode/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/generateCode/index\'; model is {page= Page:{ [Pagination { total=26 ,size=15 ,pages=2 ,current=1 }], records-size:15 }}', 390, NULL, '2018-06-19 15:49:33.119', '192.168.64.1', NULL),
	(8, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '系统配置列表', 'wang.encoding.mroot.admin.controller.system.config.ConfigController', 'index', '1d7caaf3-9ebb-491c-a3df-748511e7f98d', 'http://127.0.0.1:9901/config/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/config/index\'; model is {page= Page:{ [Pagination { total=1 ,size=15 ,pages=1 ,current=1 }], records-size:1 }}', 94, NULL, '2018-06-19 15:49:40.935', '192.168.64.1', NULL),
	(9, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '定时任务列表', 'wang.encoding.mroot.admin.controller.system.scheduleJob.ScheduleJobController', 'index', '1d7caaf3-9ebb-491c-a3df-748511e7f98d', 'http://127.0.0.1:9901/scheduleJob/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/scheduleJob/index\'; model is {page= Page:{ [Pagination { total=1 ,size=15 ,pages=1 ,current=1 }], records-size:1 }}', 257, NULL, '2018-06-19 15:49:49.004', '192.168.64.1', NULL),
	(10, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '定时任务记录列表', 'wang.encoding.mroot.admin.controller.system.scheduleJobLog.ScheduleJobLogController', 'index', '1d7caaf3-9ebb-491c-a3df-748511e7f98d', 'http://127.0.0.1:9901/scheduleJobLog/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/scheduleJobLog/index\'; model is {page= Page:{ [Pagination { total=0 ,size=15 ,pages=0 ,current=1 }], records-size:0 }}', 182, NULL, '2018-06-19 15:49:52.634', '192.168.64.1', NULL),
	(11, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '请求日志列表', 'wang.encoding.mroot.admin.controller.system.requestLog.RequestLogController', 'index', '1d7caaf3-9ebb-491c-a3df-748511e7f98d', 'http://127.0.0.1:9901/requestLog/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/requestLog/index\'; model is {page= Page:{ [Pagination { total=10 ,size=15 ,pages=1 ,current=1 }], records-size:10 }}', 24, NULL, '2018-06-19 15:49:56.407', '192.168.64.1', NULL),
	(12, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', 'logback日志列表', 'wang.encoding.mroot.admin.controller.system.loggingEvent.LoggingEventController', 'index', '1d7caaf3-9ebb-491c-a3df-748511e7f98d', 'http://127.0.0.1:9901/loggingEvent/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/loggingEvent/index\'; model is {page= Page:{ [Pagination { total=13 ,size=15 ,pages=1 ,current=1 }], records-size:13 }}', 181, NULL, '2018-06-19 15:50:05.314', '192.168.64.1', NULL),
	(13, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '文章分类列表', 'wang.encoding.mroot.admin.controller.cms.category.CategoryController', 'index', '1d7caaf3-9ebb-491c-a3df-748511e7f98d', 'http://127.0.0.1:9901/category/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/cms/category/index\'; model is {page= Page:{ [Pagination { total=1 ,size=15 ,pages=1 ,current=1 }], records-size:1 }}', 194, NULL, '2018-06-19 15:50:14.027', '192.168.64.1', NULL),
	(14, '后台管理', NULL, NULL, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '用户退出', 'wang.encoding.mroot.admin.controller.login.LoginController', 'logout', '1d7caaf3-9ebb-491c-a3df-748511e7f98d', 'http://127.0.0.1:9901/logout', 'GET', NULL, 'ModelAndView: reference to view with name \'redirect:/login\'; model is null', 57, NULL, '2018-06-19 15:50:19.487', '192.168.64.1', NULL),
	(15, '后台管理', NULL, NULL, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '用户登录页面', 'wang.encoding.mroot.admin.controller.login.LoginController', 'index', '0a38ae48-6f07-41ec-9f1c-66d7a9a3feb2', 'http://127.0.0.1:9901/login;JSESSIONID=1d7caaf3-9ebb-491c-a3df-748511e7f98d', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/login/login2Dev\'; model is {isVerifyCode=hide}', 0, NULL, '2018-06-19 15:50:19.509', '192.168.64.1', NULL),
	(16, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '处理用户登录', 'wang.encoding.mroot.admin.controller.login.LoginController', 'login', '0a38ae48-6f07-41ec-9f1c-66d7a9a3feb2', 'http://127.0.0.1:9901/doLogin', 'POST', '{username=[admin]},{password=[123456]},{formToken=[5e71adda-88a4-4446-b176-3cfd18ffbbf6]},{isVerifyCode=[hide]},{verifyCode=[]},{0=[u]},{1=[s]},{2=[e]},{3=[r]},{4=[n]},{5=[a]},{6=[m]},{7=[e]},{8=[=]},{9=[a]},{10=[d]},{11=[m]},{12=[i]},{13=[n]},{14=[&]},{15=[p]},{16=[a]},{17=[s]},{18=[s]},{19=[w]},{20=[o]},{21=[r]},{22=[d]},{23=[=]},{24=[1]},{25=[2]},{26=[3]},{27=[4]},{28=[5]},{29=[6]},{30=[&]},{31=[f]},{32=[o]},{33=[r]},{34=[m]},{35=[T]},{36=[o]},{37=[k]},{38=[e]},{39=[n]},{40=[=]},{41=[5]},{42=[e]},{43=[7]},{44=[1]},{45=[a]},{46=[d]},{47=[d]},{48=[a]},{49=[-]},{50=[8]},{51=[8]},{52=[a]},{53=[4]},{54=[-]},{55=[4]},{56=[4]},{57=[4]},{58=[6]},{59=[-]},{60=[b]},{61=[1]},{62=[7]},{63=[6]},{64=[-]},{65=[3]},{66=[c]},{67=[f]},{68=[d]},{69=[1]},{70=[8]},{71=[f]},{72=[f]},{73=[b]},{74=[b]},{75=[f]},{76=[6]},{77=[&]},{78=[i]},{79=[s]},{80=[V]},{81=[e]},{82=[r]},{83=[i]},{84=[f]},{85=[y]},{86=[C]},{87=[o]},{88=[d]},{89=[e]},{90=[=]},{91=[h]},{92=[i]},{93=[d]},{94=[e]},{95=[&]},{96=[v]},{97=[e]},{98=[r]},{99=[i]},{100=[f]},{101=[y]},{102=[C]},{103=[o]},{104=[d]},{105=[e]},{106=[=]}', '{"message":"登录成功","url":"/index","state":"ok"}', 13, NULL, '2018-06-19 15:50:20.974', '192.168.64.1', NULL),
	(17, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '主页', 'wang.encoding.mroot.admin.controller.index.IndexController', 'index', '0a38ae48-6f07-41ec-9f1c-66d7a9a3feb2', 'http://127.0.0.1:9901/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/index/index\'; model is {osInfoMap={操作系统信息=Windows 10, 操作版本=10.0, 架构=amd64, Java的运行环境版本=10.0.1, Java运行时环境规范版本=10, Java的虚拟机规范版本=10, Java的类格式版本号=54.0}}', 30, NULL, '2018-06-19 15:50:21.069', '192.168.64.1', NULL),
	(18, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '文章列表', 'wang.encoding.mroot.admin.controller.cms.article.ArticleController', 'index', '0a38ae48-6f07-41ec-9f1c-66d7a9a3feb2', 'http://127.0.0.1:9901/article/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/cms/article/index\'; model is {page= Page:{ [Pagination { total=1 ,size=15 ,pages=1 ,current=1 }], records-size:1 }}', 119, NULL, '2018-06-19 15:50:24.247', '192.168.64.1', NULL),
	(19, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '博客列表', 'wang.encoding.mroot.admin.controller.cms.blog.BlogController', 'index', '0a38ae48-6f07-41ec-9f1c-66d7a9a3feb2', 'http://127.0.0.1:9901/blog/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/cms/blog/index\'; model is {page= Page:{ [Pagination { total=1 ,size=15 ,pages=1 ,current=1 }], records-size:1 }}', 169, NULL, '2018-06-19 15:50:27.355', '192.168.64.1', NULL),
	(20, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '用户列表', 'wang.encoding.mroot.admin.controller.system.user.UserController', 'index', '0a38ae48-6f07-41ec-9f1c-66d7a9a3feb2', 'http://127.0.0.1:9901/user/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/user/index\'; model is {page= Page:{ [Pagination { total=26 ,size=15 ,pages=2 ,current=1 }], records-size:15 }}', 307, NULL, '2018-06-19 15:53:24.527', '192.168.64.1', NULL),
	(21, '后台管理', NULL, NULL, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '用户登录页面', 'wang.encoding.mroot.admin.controller.login.LoginController', 'index', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/login;JSESSIONID=643b5cd0-ba72-4980-8b7c-64f48d59492b', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/login/login2Dev\'; model is {isVerifyCode=hide}', 129, NULL, '2018-06-19 16:35:06.725', '192.168.64.1', NULL),
	(22, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '处理用户登录', 'wang.encoding.mroot.admin.controller.login.LoginController', 'login', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/doLogin', 'POST', '{username=[admin]},{password=[123456]},{formToken=[50a72bdd-87e7-413c-8d2e-ca92c05987c3]},{isVerifyCode=[hide]},{verifyCode=[]},{0=[u]},{1=[s]},{2=[e]},{3=[r]},{4=[n]},{5=[a]},{6=[m]},{7=[e]},{8=[=]},{9=[a]},{10=[d]},{11=[m]},{12=[i]},{13=[n]},{14=[&]},{15=[p]},{16=[a]},{17=[s]},{18=[s]},{19=[w]},{20=[o]},{21=[r]},{22=[d]},{23=[=]},{24=[1]},{25=[2]},{26=[3]},{27=[4]},{28=[5]},{29=[6]},{30=[&]},{31=[f]},{32=[o]},{33=[r]},{34=[m]},{35=[T]},{36=[o]},{37=[k]},{38=[e]},{39=[n]},{40=[=]},{41=[5]},{42=[0]},{43=[a]},{44=[7]},{45=[2]},{46=[b]},{47=[d]},{48=[d]},{49=[-]},{50=[8]},{51=[7]},{52=[e]},{53=[7]},{54=[-]},{55=[4]},{56=[1]},{57=[3]},{58=[c]},{59=[-]},{60=[8]},{61=[d]},{62=[2]},{63=[e]},{64=[-]},{65=[c]},{66=[a]},{67=[9]},{68=[2]},{69=[c]},{70=[0]},{71=[5]},{72=[9]},{73=[8]},{74=[7]},{75=[c]},{76=[3]},{77=[&]},{78=[i]},{79=[s]},{80=[V]},{81=[e]},{82=[r]},{83=[i]},{84=[f]},{85=[y]},{86=[C]},{87=[o]},{88=[d]},{89=[e]},{90=[=]},{91=[h]},{92=[i]},{93=[d]},{94=[e]},{95=[&]},{96=[v]},{97=[e]},{98=[r]},{99=[i]},{100=[f]},{101=[y]},{102=[C]},{103=[o]},{104=[d]},{105=[e]},{106=[=]}', '{"message":"登录成功","url":"/user/index","state":"ok"}', 16, NULL, '2018-06-19 16:35:56.138', '192.168.64.1', NULL),
	(23, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '用户列表', 'wang.encoding.mroot.admin.controller.system.user.UserController', 'index', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/user/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/user/index\'; model is {page= Page:{ [Pagination { total=26 ,size=15 ,pages=2 ,current=1 }], records-size:15 }}', 237, NULL, '2018-06-19 16:35:56.790', '192.168.64.1', NULL),
	(24, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '文章列表', 'wang.encoding.mroot.admin.controller.cms.article.ArticleController', 'index', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/article/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/cms/article/index\'; model is {page= Page:{ [Pagination { total=1 ,size=15 ,pages=1 ,current=1 }], records-size:1 }}', 396, NULL, '2018-06-19 16:36:09.021', '192.168.64.1', NULL),
	(25, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '修改文章', 'wang.encoding.mroot.admin.controller.cms.article.ArticleController', 'edit', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/article/edit/1', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/cms/article/edit\'; model is {article=Article(id=1, categoryId=1, title=为偶奇偶文件佛我就哦较为哦if那我我金佛is阿丹姐of撒旦教哦发送到我就胸肌, description=fadsff, cover=/assets/img/article/20180619/20180619110012613390409979616353.jpg, sort=1, view=0, level=1, linkUri=null, show=1, status=1, gmtCreate=Thu Jun 07 10:25:30 CST 2018, gmtCreateIp=192.168.64.1, gmtModified=Tue Jun 19 11:07:08 CST 2018, articleContent=null), articleContent=ArticleContent(id=1, content=<p>fsdfsadf<img src="http://127.0.0.1:9901/assets/upload/20180607/201806071025241095374.jpg" alt="1107242301e6e31c8defa699cc.jpg">fsd</p>, contentHtml=<p>fsdfsadf<img src="http://127.0.0.1:9901/assets/upload/20180607/201806071025241095374.jpg" alt="1107242301e6e31c8defa699cc.jpg">fsd</p>, gmtCreate=Thu Jun 07 10:25:31 CST 2018, gmtCreateIp=192.168.64.1, gmtModified=Tue Jun 19 11:07:08 CST 2018, status=1)}', 209, NULL, '2018-06-19 16:36:15.078', '192.168.64.1', NULL),
	(26, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '恢复文章', 'wang.encoding.mroot.admin.controller.cms.article.ArticleController', 'uploadCover', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/article/uploadCover', 'POST', '{id=[WU_FILE_0]},{name=[1107242301e6e31c8defa699cc.jpg]},{type=[image/jpeg]},{lastModifiedDate=[2018/6/19 下午4:36:50]},{size=[95158]}', '{"message":"/assets/img/article/20180619/201806191636506741497191561172811.jpg","state":"ok"}', 83, NULL, '2018-06-19 16:36:50.715', '192.168.64.1', NULL),
	(27, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '更新修改文章', 'wang.encoding.mroot.admin.controller.cms.article.ArticleController', 'update', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/article/update', 'POST', '{id=[1]},{formToken=[9239a359-9e48-4ca1-af17-b5714e5aad4c]},{refererUrl=[http://127.0.0.1:9901/article/index]},{categoryId=[1]},{title=[系统简介]},{description=[系统简介]},{statusStr=[on]},{cover=[/assets/img/article/20180619/201806191636506741497191561172811.jpg]},{showStr=[on]},{level=[1]},{sort=[1]},{linkUri=[]},{content=[<pre style="background-color:#2b2b2b;color:#a9b7c6;font-family:&#39;宋体&#39;;font-size:13.5pt;">MRoot项目简介\r\n\r\nMRoot采用Spring&nbsp;Boot2整合其他开源框架架构，使用Kotlin语言开发的通用后台管理系统（100%兼容Java,可以与Java互操作）\r\n\r\n代码生成器，可以生成业务90%的代码，可快速完成某个业务的开发\r\n\r\n使用Maven对项目进行模块化管理，提高项目的易开发性、扩展性\r\n\r\n\r\n具有如下特点\r\n\r\n灵活的权限控制，可控制权限到按钮级别\r\n\r\n完善的角色管理及数据权限\r\n\r\n完善的XSS防范及脚本过滤，彻底杜绝XSS攻击\r\n\r\n友好的代码结构及注释，便于阅读及二次开发\r\n\r\nQuartz定时任务，可动态完成任务的添加、修改、删除、暂停、恢复及日志查看等功能\r\n\r\n前端使用Bootstrap，优美的页面，丰富的插件\r\n\r\n主要功能\r\n\r\n数据库：Druid数据库连接池，监控数据库访问性能，统计SQL的执行性能\r\n\r\n持久层：MyBatis持久化，使用MyBatis-Plus优化，减少sql开发量，使用Hibernate&nbsp;Validation进行数据验证\r\n\r\nMVC：基于Spring&nbsp;Mvc注解，Rest风格Controller，Exception统一管理\r\n\r\n任务调度：Spring+Quartz,&nbsp;可动态完成任务的添加、修改、删除、暂停、恢复及日志查看等功能\r\n\r\n基于Spring的国际化信息\r\n\r\nShiro进行权限控制\r\n\r\n缓存：注解缓存数据\r\n\r\n自定义线程池、异步任务\r\n\r\n日志：logback打印日志，存入数据库，同时基于时间和文件大小分割日志文件\r\n\r\n工具类：加密解密、字符串处理等等\r\n\r\n\r\n技术选型\r\n\r\n开发语言：Kotlin\r\n\r\n核心框架：Spring&nbsp;Boot2\r\n\r\n数据库连接池：Alibaba&nbsp;Druid\r\n\r\n持久层框架：MyBatis&nbsp;+&nbsp;MyBatis-Plus\r\n\r\n安全框架：Apache&nbsp;Shiro\r\n\r\n任务调度：Spring&nbsp;+&nbsp;Quartz\r\n\r\n缓存框架：Ehcache3\r\n\r\n日志管理：SLF4J、Logback\r\n\r\n验证框架：Hibernate&nbsp;Validation\r\n\r\n模板：Freemarker\r\n\r\n前端框架：Bootstrap\r\n\r\n开发环境\r\n\r\nJDK10+、Maven3.5.2+、Mysql8+、Spring&nbsp;Boot2\r\n\r\n本地部署\r\n\r\n通过git下载源码\r\n\r\n创建数据库mroot，数据库编码为UTF-8\r\n\r\n执行db/mroot.sql文件】\r\n\r\n修改application-XXX.properties文件，更新MySQL账号和密码\r\n\r\nIDEA运行MRootAdminApplication.kt，则可启动项目\r\n\r\nmroot-admin访问路径：你的IP地址:端口/login\r\n\r\n账号密码：admin/123456\r\n\r\n\r\n\r\n已完成功能\r\n\r\n\r\n用户管理：配置用户角色\r\n\r\n角色管理：配置角色所拥有的权限\r\n\r\n菜单管理：配置角色所拥有的权限\r\n\r\n操作日志：系统操作日志记录和查询\r\n\r\n代码生成：可生成90%的业务代码\r\n\r\n缓存管理：Ehcache3\r\n\r\n文章管理：百度编辑器的上传处理</pre><p><br/></p>]}', '{"formToken":"a2e6e149-bee4-4217-968c-f3c3221ef0a5","message":"修改成功","state":"ok"}', 339, NULL, '2018-06-19 16:36:52.808', '192.168.64.1', NULL),
	(28, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '博客列表', 'wang.encoding.mroot.admin.controller.cms.blog.BlogController', 'index', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/blog/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/cms/blog/index\'; model is {page= Page:{ [Pagination { total=1 ,size=15 ,pages=1 ,current=1 }], records-size:1 }}', 18, NULL, '2018-06-19 16:37:03.334', '192.168.64.1', NULL),
	(29, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '角色列表', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'index', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/role/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/role/index\'; model is {page= Page:{ [Pagination { total=3 ,size=15 ,pages=1 ,current=1 }], records-size:3 }}', 53, NULL, '2018-06-19 16:37:31.079', '192.168.64.1', NULL),
	(30, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '修改角色', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'edit', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/role/edit/2', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/role/edit\'; model is {role=Role(id=2, name=test, title=test, status=1, gmtCreate=Mon Mar 19 15:09:36 CST 2018, gmtCreateIp=127.0.0.1, gmtModified=Wed Mar 28 22:58:24 CST 2018, sort=2, remark=测试用户查看权限)}', 218, NULL, '2018-06-19 16:37:39.306', '192.168.64.1', NULL),
	(31, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '更新修改角色', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'update', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/role/update', 'POST', '{formToken=[81539101-7e89-4482-9844-378d74ab57d6]},{refererUrl=[http://127.0.0.1:9901/role/index]},{id=[2]},{name=[test]},{title=[访客]},{statusStr=[on]},{sort=[2]},{remark=[测试用户查看权限]}', '{"formToken":"cbfea2f2-1df5-4d15-aff9-d7af2f5cd830","message":"修改成功","state":"ok"}', 49, NULL, '2018-06-19 16:38:00.668', '192.168.64.1', NULL),
	(32, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '修改角色', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'edit', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/role/edit/2', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/role/edit\'; model is {role=Role(id=2, name=test, title=访客, status=1, gmtCreate=Mon Mar 19 15:09:36 CST 2018, gmtCreateIp=127.0.0.1, gmtModified=Tue Jun 19 16:38:00 CST 2018, sort=2, remark=测试用户查看权限)}', 88, NULL, '2018-06-19 16:38:02.105', '192.168.64.1', NULL),
	(33, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '更新修改角色', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'update', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/role/update', 'POST', '{formToken=[a1240b4a-3866-45fe-a4f9-b73eb8466bc8]},{refererUrl=[http://127.0.0.1:9901/role/index]},{id=[2]},{name=[test]},{title=[访客]},{statusStr=[on]},{sort=[2]},{remark=[访客拥有查看权限]}', '{"formToken":"195c55ca-ab23-44fd-8cd1-23c427acb43c","message":"修改成功","state":"ok"}', 35, NULL, '2018-06-19 16:38:14.281', '192.168.64.1', NULL),
	(34, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '修改角色', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'edit', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/role/edit/2', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/role/edit\'; model is {role=Role(id=2, name=test, title=访客, status=1, gmtCreate=Mon Mar 19 15:09:36 CST 2018, gmtCreateIp=127.0.0.1, gmtModified=Tue Jun 19 16:38:14 CST 2018, sort=2, remark=访客拥有查看权限)}', 7, NULL, '2018-06-19 16:38:15.130', '192.168.64.1', NULL),
	(35, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '角色列表', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'index', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/role/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/role/index\'; model is {page= Page:{ [Pagination { total=3 ,size=15 ,pages=1 ,current=1 }], records-size:3 }}', 196, NULL, '2018-06-19 16:38:19.093', '192.168.64.1', NULL),
	(36, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '修改角色', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'edit', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/role/edit/3', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/role/edit\'; model is {role=Role(id=3, name=DEV, title=开发者, status=1, gmtCreate=Mon Jun 04 10:16:39 CST 2018, gmtCreateIp=127.0.0.1, gmtModified=Sat Jun 09 13:40:02 CST 2018, sort=3, remark=开发者)}', 91, NULL, '2018-06-19 16:38:28.626', '192.168.64.1', NULL),
	(37, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '角色列表', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'index', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/role/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/role/index\'; model is {page= Page:{ [Pagination { total=3 ,size=15 ,pages=1 ,current=1 }], records-size:3 }}', 49, NULL, '2018-06-19 16:38:37.465', '192.168.64.1', NULL),
	(38, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '修改角色', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'edit', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/role/edit/2', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/role/edit\'; model is {role=Role(id=2, name=test, title=访客, status=1, gmtCreate=Mon Mar 19 15:09:36 CST 2018, gmtCreateIp=127.0.0.1, gmtModified=Tue Jun 19 16:38:14 CST 2018, sort=2, remark=访客拥有查看权限)}', 165, NULL, '2018-06-19 16:38:42.193', '192.168.64.1', NULL),
	(39, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '更新修改角色', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'update', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/role/update', 'POST', '{formToken=[79427698-d8be-4fcc-95c9-0112bd674b3a]},{refererUrl=[http://127.0.0.1:9901/role/index]},{id=[2]},{name=[TEST]},{title=[访客]},{statusStr=[on]},{sort=[2]},{remark=[访客拥有查看权限]}', '{"formToken":"ac0c5244-0f33-4cf1-a128-5818327fe56f","message":"修改成功","state":"ok"}', 27, NULL, '2018-06-19 16:38:47.918', '192.168.64.1', NULL),
	(40, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '修改角色', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'edit', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/role/edit/2', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/role/edit\'; model is {role=Role(id=2, name=TEST, title=访客, status=1, gmtCreate=Mon Mar 19 15:09:36 CST 2018, gmtCreateIp=127.0.0.1, gmtModified=Tue Jun 19 16:38:47 CST 2018, sort=2, remark=访客拥有查看权限)}', 133, NULL, '2018-06-19 16:38:49.053', '192.168.64.1', NULL),
	(41, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '角色列表', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'index', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/role/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/role/index\'; model is {page= Page:{ [Pagination { total=3 ,size=15 ,pages=1 ,current=1 }], records-size:3 }}', 10, NULL, '2018-06-19 16:38:50.444', '192.168.64.1', NULL),
	(42, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '修改角色', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'edit', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/role/edit/1', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/role/edit\'; model is {role=Role(id=1, name=admin, title=超级管理员, status=1, gmtCreate=Sat Feb 24 16:03:36 CST 2018, gmtCreateIp=127.0.0.1, gmtModified=null, sort=1, remark=超级管理员拥有最高权限)}', 111, NULL, '2018-06-19 16:38:56.413', '192.168.64.1', NULL),
	(43, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '更新修改角色', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'update', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/role/update', 'POST', '{formToken=[87e7765a-ac95-4c51-bd61-56caa893adc6]},{refererUrl=[http://127.0.0.1:9901/role/index]},{id=[1]},{name=[ADMIN]},{title=[超级管理员]},{statusStr=[on]},{sort=[1]},{remark=[超级管理员拥有最高权限]}', '{"formToken":"eea0dad3-ea70-40bb-ad1d-65d4b42e8698","message":"修改成功","state":"ok"}', 53, NULL, '2018-06-19 16:39:06.739', '192.168.64.1', NULL),
	(44, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '修改角色', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'edit', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/role/edit/1', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/role/edit\'; model is {role=Role(id=1, name=ADMIN, title=超级管理员, status=1, gmtCreate=Sat Feb 24 16:03:36 CST 2018, gmtCreateIp=127.0.0.1, gmtModified=Tue Jun 19 16:39:06 CST 2018, sort=1, remark=超级管理员拥有最高权限)}', 8, NULL, '2018-06-19 16:39:08.684', '192.168.64.1', NULL),
	(45, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '角色列表', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'index', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/role/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/role/index\'; model is {page= Page:{ [Pagination { total=3 ,size=15 ,pages=1 ,current=1 }], records-size:3 }}', 129, NULL, '2018-06-19 16:39:15.607', '192.168.64.1', NULL),
	(46, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '角色授权', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'authorization', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/role/authorization/2', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/role/authorization\'; model is {role=Role(id=2, name=TEST, title=访客, status=1, gmtCreate=Mon Mar 19 15:09:36 CST 2018, gmtCreateIp=127.0.0.1, gmtModified=Tue Jun 19 16:38:47 CST 2018, sort=2, remark=访客拥有查看权限)}', 158, NULL, '2018-06-19 16:39:19.845', '192.168.64.1', NULL),
	(47, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '保存角色授权', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'authorizationSave', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/role/authorizationSave', 'POST', '{formToken=[9a106520-bf08-4f20-8373-2598ff3af3e1]},{refererUrl=[http://127.0.0.1:9901/role/index]},{id=[2]},{rules=[2,3,4,5,6,8,12,13,15,19,20,22,23,24,26,30,33,34,36,37,39,43,45,46,48,49,50,51,52,53,54,55,57,62,63,66,67,68,70,75,77,78,81,82,83,84,85,86,87,88,89,90,91,92,94,98,99,100,102,103,104,106,108,111,112,117,118]}', '{"formToken":"b6f37590-c868-45c0-906f-486a6b6a43db","message":"授权成功","state":"ok"}', 97, NULL, '2018-06-19 16:41:14.566', '192.168.64.1', NULL),
	(48, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '角色授权', 'wang.encoding.mroot.admin.controller.system.role.RoleController', 'authorization', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/role/authorization/2', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/role/authorization\'; model is {ruleString=2,3,4,5,6,8,12,13,15,19,20,22,23,24,26,30,33,34,36,37,39,43,45,46,48,49,50,51,52,53,54,55,57,62,63,66,67,68,70,75,77,78,81,82,83,84,85,86,87,88,89,90,91,92,94,98,99,100,102,103,104,106,108,111,112,117,118, role=Role(id=2, name=TEST, title=访客, status=1, gmtCreate=Mon Mar 19 15:09:36 CST 2018, gmtCreateIp=127.0.0.1, gmtModified=Tue Jun 19 16:38:47 CST 2018, sort=2, remark=访客拥有查看权限)}', 115, NULL, '2018-06-19 16:41:15.748', '192.168.64.1', NULL),
	(49, '后台管理', NULL, NULL, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '用户退出', 'wang.encoding.mroot.admin.controller.login.LoginController', 'logout', '643b5cd0-ba72-4980-8b7c-64f48d59492b', 'http://127.0.0.1:9901/logout', 'GET', NULL, 'ModelAndView: reference to view with name \'redirect:/login\'; model is null', 3, NULL, '2018-06-19 16:41:19.922', '192.168.64.1', NULL),
	(50, '后台管理', NULL, NULL, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '用户登录页面', 'wang.encoding.mroot.admin.controller.login.LoginController', 'index', '560046f2-5ef5-49e5-83c3-ffea77a020fe', 'http://127.0.0.1:9901/login;JSESSIONID=643b5cd0-ba72-4980-8b7c-64f48d59492b', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/login/login2Dev\'; model is {isVerifyCode=hide}', 0, NULL, '2018-06-19 16:41:19.940', '192.168.64.1', NULL),
	(51, '后台管理', 9, '123456', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '处理用户登录', 'wang.encoding.mroot.admin.controller.login.LoginController', 'login', '560046f2-5ef5-49e5-83c3-ffea77a020fe', 'http://127.0.0.1:9901/doLogin', 'POST', '{username=[123456]},{password=[123456]},{formToken=[6ab02f36-07a0-47ef-81e0-04718879a824]},{isVerifyCode=[hide]},{verifyCode=[]},{0=[u]},{1=[s]},{2=[e]},{3=[r]},{4=[n]},{5=[a]},{6=[m]},{7=[e]},{8=[=]},{9=[1]},{10=[2]},{11=[3]},{12=[4]},{13=[5]},{14=[6]},{15=[&]},{16=[p]},{17=[a]},{18=[s]},{19=[s]},{20=[w]},{21=[o]},{22=[r]},{23=[d]},{24=[=]},{25=[1]},{26=[2]},{27=[3]},{28=[4]},{29=[5]},{30=[6]},{31=[&]},{32=[f]},{33=[o]},{34=[r]},{35=[m]},{36=[T]},{37=[o]},{38=[k]},{39=[e]},{40=[n]},{41=[=]},{42=[6]},{43=[a]},{44=[b]},{45=[0]},{46=[2]},{47=[f]},{48=[3]},{49=[6]},{50=[-]},{51=[0]},{52=[7]},{53=[a]},{54=[0]},{55=[-]},{56=[4]},{57=[7]},{58=[e]},{59=[f]},{60=[-]},{61=[8]},{62=[1]},{63=[e]},{64=[0]},{65=[-]},{66=[0]},{67=[4]},{68=[7]},{69=[1]},{70=[8]},{71=[8]},{72=[7]},{73=[9]},{74=[a]},{75=[8]},{76=[2]},{77=[4]},{78=[&]},{79=[i]},{80=[s]},{81=[V]},{82=[e]},{83=[r]},{84=[i]},{85=[f]},{86=[y]},{87=[C]},{88=[o]},{89=[d]},{90=[e]},{91=[=]},{92=[h]},{93=[i]},{94=[d]},{95=[e]},{96=[&]},{97=[v]},{98=[e]},{99=[r]},{100=[i]},{101=[f]},{102=[y]},{103=[C]},{104=[o]},{105=[d]},{106=[e]},{107=[=]}', '{"message":"登录成功","url":"/index","state":"ok"}', 12, NULL, '2018-06-19 16:41:24.489', '192.168.64.1', NULL),
	(52, '后台管理', 9, '123456', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '主页', 'wang.encoding.mroot.admin.controller.index.IndexController', 'index', '560046f2-5ef5-49e5-83c3-ffea77a020fe', 'http://127.0.0.1:9901/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/index/index\'; model is {osInfoMap={操作系统信息=Windows 10, 操作版本=10.0, 架构=amd64, Java的运行环境版本=10.0.1, Java运行时环境规范版本=10, Java的虚拟机规范版本=10, Java的类格式版本号=54.0}}', 42, NULL, '2018-06-19 16:41:24.704', '192.168.64.1', NULL),
	(53, '后台管理', 9, '123456', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '用户列表', 'wang.encoding.mroot.admin.controller.system.user.UserController', 'index', '560046f2-5ef5-49e5-83c3-ffea77a020fe', 'http://127.0.0.1:9901/user/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/user/index\'; model is {page= Page:{ [Pagination { total=26 ,size=15 ,pages=2 ,current=1 }], records-size:15 }}', 149, NULL, '2018-06-19 16:41:32.832', '192.168.64.1', NULL),
	(54, '后台管理', 9, '123456', '管理员', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '请求日志列表', 'wang.encoding.mroot.admin.controller.system.requestLog.RequestLogController', 'index', '560046f2-5ef5-49e5-83c3-ffea77a020fe', 'http://127.0.0.1:9901/requestLog/index', 'GET', NULL, 'ModelAndView: reference to view with name \'/default/system/requestLog/index\'; model is {page= Page:{ [Pagination { total=53 ,size=15 ,pages=4 ,current=1 }], records-size:15 }}', 122, NULL, '2018-06-19 16:41:51.533', '192.168.64.1', NULL);
/*!40000 ALTER TABLE `system_request_log` ENABLE KEYS */;

-- 导出  表 mroot.system_role 结构
CREATE TABLE IF NOT EXISTS `system_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id编号',
  `name` varchar(80) NOT NULL COMMENT '角色标识',
  `title` varchar(80) NOT NULL COMMENT '角色名称',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '状态(1是正常，2是禁用，3是删除)',
  `gmt_create` datetime(3) NOT NULL COMMENT '创建时间',
  `gmt_create_ip` varchar(64) NOT NULL COMMENT '创建IP',
  `gmt_modified` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `remark` varchar(200) DEFAULT NULL COMMENT '角色备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  UNIQUE KEY `uk_title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- 正在导出表  mroot.system_role 的数据：~3 rows (大约)
DELETE FROM `system_role`;
/*!40000 ALTER TABLE `system_role` DISABLE KEYS */;
INSERT INTO `system_role` (`id`, `name`, `title`, `status`, `gmt_create`, `gmt_create_ip`, `gmt_modified`, `sort`, `remark`) VALUES
	(1, 'ADMIN', '超级管理员', 1, '2018-02-24 16:03:36.000', '127.0.0.1', '2018-06-19 16:39:06.690', 1, '超级管理员拥有最高权限'),
	(2, 'TEST', '访客', 1, '2018-03-19 15:09:36.331', '127.0.0.1', '2018-06-19 16:38:47.895', 2, '访客拥有查看权限'),
	(3, 'DEV', '开发者', 1, '2018-06-04 10:16:39.816', '127.0.0.1', '2018-06-09 13:40:02.820', 3, '开发者');
/*!40000 ALTER TABLE `system_role` ENABLE KEYS */;

-- 导出  表 mroot.system_role_rule 结构
CREATE TABLE IF NOT EXISTS `system_role_rule` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id编号',
  `role_id` bigint(20) unsigned NOT NULL COMMENT '角色id',
  `rule_id` bigint(20) unsigned NOT NULL COMMENT '权限id',
  `gmt_create` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_roleId` (`role_id`),
  KEY `idx_ruleId` (`rule_id`)
) ENGINE=InnoDB AUTO_INCREMENT=187 DEFAULT CHARSET=utf8 COMMENT='角色对应权限表';

-- 正在导出表  mroot.system_role_rule 的数据：~185 rows (大约)
DELETE FROM `system_role_rule`;
/*!40000 ALTER TABLE `system_role_rule` DISABLE KEYS */;
INSERT INTO `system_role_rule` (`id`, `role_id`, `rule_id`, `gmt_create`) VALUES
	(1, 1, 1, '2018-06-14 20:18:10.285'),
	(2, 1, 2, '2018-06-14 20:18:10.285'),
	(3, 1, 3, '2018-06-14 20:18:10.285'),
	(4, 1, 4, '2018-06-14 20:18:10.285'),
	(5, 1, 5, '2018-06-14 20:18:10.285'),
	(6, 1, 6, '2018-06-14 20:18:10.285'),
	(7, 1, 7, '2018-06-14 20:18:10.285'),
	(8, 1, 8, '2018-06-14 20:18:10.285'),
	(9, 1, 9, '2018-06-14 20:18:10.285'),
	(10, 1, 10, '2018-06-14 20:18:10.285'),
	(11, 1, 11, '2018-06-14 20:18:10.285'),
	(12, 1, 12, '2018-06-14 20:18:10.285'),
	(13, 1, 13, '2018-06-14 20:18:10.285'),
	(14, 1, 14, '2018-06-14 20:18:10.285'),
	(15, 1, 15, '2018-06-14 20:18:10.285'),
	(16, 1, 16, '2018-06-14 20:18:10.285'),
	(17, 1, 17, '2018-06-14 20:18:10.285'),
	(18, 1, 18, '2018-06-14 20:18:10.285'),
	(19, 1, 19, '2018-06-14 20:18:10.285'),
	(20, 1, 20, '2018-06-14 20:18:10.285'),
	(21, 1, 21, '2018-06-14 20:18:10.285'),
	(22, 1, 22, '2018-06-14 20:18:10.285'),
	(23, 1, 23, '2018-06-14 20:18:10.285'),
	(24, 1, 24, '2018-06-14 20:18:10.285'),
	(25, 1, 25, '2018-06-14 20:18:10.285'),
	(26, 1, 26, '2018-06-14 20:18:10.285'),
	(27, 1, 27, '2018-06-14 20:18:10.285'),
	(28, 1, 28, '2018-06-14 20:18:10.285'),
	(29, 1, 29, '2018-06-14 20:18:10.285'),
	(30, 1, 30, '2018-06-14 20:18:10.285'),
	(31, 1, 31, '2018-06-14 20:18:10.285'),
	(32, 1, 32, '2018-06-14 20:18:10.285'),
	(33, 1, 33, '2018-06-14 20:18:10.285'),
	(34, 1, 34, '2018-06-14 20:18:10.285'),
	(35, 1, 35, '2018-06-14 20:18:10.285'),
	(36, 1, 36, '2018-06-14 20:18:10.285'),
	(37, 1, 37, '2018-06-14 20:18:10.285'),
	(38, 1, 38, '2018-06-14 20:18:10.285'),
	(39, 1, 39, '2018-06-14 20:18:10.285'),
	(40, 1, 40, '2018-06-14 20:18:10.285'),
	(41, 1, 41, '2018-06-14 20:18:10.285'),
	(42, 1, 42, '2018-06-14 20:18:10.285'),
	(43, 1, 43, '2018-06-14 20:18:10.285'),
	(44, 1, 44, '2018-06-14 20:18:10.285'),
	(45, 1, 45, '2018-06-14 20:18:10.285'),
	(46, 1, 46, '2018-06-14 20:18:10.285'),
	(47, 1, 47, '2018-06-14 20:18:10.285'),
	(48, 1, 48, '2018-06-14 20:18:10.285'),
	(49, 1, 49, '2018-06-14 20:18:10.285'),
	(50, 1, 50, '2018-06-14 20:18:10.285'),
	(51, 1, 51, '2018-06-14 20:18:10.285'),
	(52, 1, 52, '2018-06-14 20:18:10.285'),
	(53, 1, 53, '2018-06-14 20:18:10.285'),
	(54, 1, 54, '2018-06-14 20:18:10.285'),
	(55, 1, 55, '2018-06-14 20:18:10.285'),
	(56, 1, 56, '2018-06-14 20:18:10.285'),
	(57, 1, 57, '2018-06-14 20:18:10.285'),
	(58, 1, 58, '2018-06-14 20:18:10.285'),
	(59, 1, 59, '2018-06-14 20:18:10.285'),
	(60, 1, 60, '2018-06-14 20:18:10.285'),
	(61, 1, 61, '2018-06-14 20:18:10.285'),
	(62, 1, 62, '2018-06-14 20:18:10.285'),
	(63, 1, 63, '2018-06-14 20:18:10.285'),
	(64, 1, 64, '2018-06-14 20:18:10.285'),
	(65, 1, 65, '2018-06-14 20:18:10.285'),
	(66, 1, 66, '2018-06-14 20:18:10.285'),
	(67, 1, 67, '2018-06-14 20:18:10.285'),
	(68, 1, 68, '2018-06-14 20:18:10.285'),
	(69, 1, 69, '2018-06-14 20:18:10.285'),
	(70, 1, 70, '2018-06-14 20:18:10.285'),
	(71, 1, 71, '2018-06-14 20:18:10.285'),
	(72, 1, 72, '2018-06-14 20:18:10.285'),
	(73, 1, 73, '2018-06-14 20:18:10.285'),
	(74, 1, 74, '2018-06-14 20:18:10.285'),
	(75, 1, 75, '2018-06-14 20:18:10.285'),
	(76, 1, 76, '2018-06-14 20:18:10.285'),
	(77, 1, 77, '2018-06-14 20:18:10.285'),
	(78, 1, 78, '2018-06-14 20:18:10.285'),
	(79, 1, 79, '2018-06-14 20:18:10.285'),
	(80, 1, 80, '2018-06-14 20:18:10.285'),
	(81, 1, 81, '2018-06-14 20:18:10.285'),
	(82, 1, 82, '2018-06-14 20:18:10.285'),
	(83, 1, 83, '2018-06-14 20:18:10.285'),
	(84, 1, 84, '2018-06-14 20:18:10.285'),
	(85, 1, 85, '2018-06-14 20:18:10.285'),
	(86, 1, 86, '2018-06-14 20:18:10.285'),
	(87, 1, 87, '2018-06-14 20:18:10.285'),
	(88, 1, 88, '2018-06-14 20:18:10.285'),
	(89, 1, 89, '2018-06-14 20:18:10.285'),
	(90, 1, 90, '2018-06-14 20:18:10.285'),
	(91, 1, 91, '2018-06-14 20:18:10.285'),
	(92, 1, 92, '2018-06-14 20:18:10.285'),
	(93, 1, 93, '2018-06-14 20:18:10.285'),
	(94, 1, 94, '2018-06-14 20:18:10.285'),
	(95, 1, 95, '2018-06-14 20:18:10.285'),
	(96, 1, 96, '2018-06-14 20:18:10.285'),
	(97, 1, 97, '2018-06-14 20:18:10.285'),
	(98, 1, 98, '2018-06-14 20:18:10.285'),
	(99, 1, 99, '2018-06-14 20:18:10.285'),
	(100, 1, 100, '2018-06-14 20:18:10.285'),
	(101, 1, 101, '2018-06-14 20:18:10.285'),
	(102, 1, 102, '2018-06-14 20:18:10.285'),
	(103, 1, 103, '2018-06-14 20:18:10.285'),
	(104, 1, 104, '2018-06-14 20:18:10.285'),
	(105, 1, 105, '2018-06-14 20:18:10.285'),
	(106, 1, 106, '2018-06-14 20:18:10.285'),
	(107, 1, 107, '2018-06-14 20:18:10.285'),
	(108, 1, 108, '2018-06-14 20:18:10.285'),
	(109, 1, 109, '2018-06-14 20:18:10.285'),
	(110, 1, 110, '2018-06-14 20:18:10.285'),
	(111, 1, 111, '2018-06-14 20:18:10.285'),
	(112, 1, 112, '2018-06-14 20:18:10.285'),
	(113, 1, 113, '2018-06-14 20:18:10.285'),
	(114, 1, 114, '2018-06-14 20:18:10.285'),
	(115, 1, 115, '2018-06-14 20:18:10.285'),
	(116, 1, 116, '2018-06-14 20:18:10.285'),
	(117, 1, 117, '2018-06-14 20:18:10.285'),
	(118, 1, 118, '2018-06-19 15:37:16.000'),
	(119, 2, 1, '2018-06-19 16:41:14.525'),
	(120, 2, 2, '2018-06-19 16:41:14.525'),
	(121, 2, 3, '2018-06-19 16:41:14.525'),
	(122, 2, 4, '2018-06-19 16:41:14.525'),
	(123, 2, 5, '2018-06-19 16:41:14.525'),
	(124, 2, 6, '2018-06-19 16:41:14.525'),
	(125, 2, 8, '2018-06-19 16:41:14.525'),
	(126, 2, 12, '2018-06-19 16:41:14.525'),
	(127, 2, 13, '2018-06-19 16:41:14.525'),
	(128, 2, 15, '2018-06-19 16:41:14.525'),
	(129, 2, 19, '2018-06-19 16:41:14.525'),
	(130, 2, 20, '2018-06-19 16:41:14.525'),
	(131, 2, 22, '2018-06-19 16:41:14.525'),
	(132, 2, 23, '2018-06-19 16:41:14.525'),
	(133, 2, 24, '2018-06-19 16:41:14.525'),
	(134, 2, 26, '2018-06-19 16:41:14.525'),
	(135, 2, 30, '2018-06-19 16:41:14.525'),
	(136, 2, 33, '2018-06-19 16:41:14.525'),
	(137, 2, 34, '2018-06-19 16:41:14.525'),
	(138, 2, 36, '2018-06-19 16:41:14.525'),
	(139, 2, 37, '2018-06-19 16:41:14.525'),
	(140, 2, 39, '2018-06-19 16:41:14.525'),
	(141, 2, 43, '2018-06-19 16:41:14.525'),
	(142, 2, 45, '2018-06-19 16:41:14.525'),
	(143, 2, 46, '2018-06-19 16:41:14.525'),
	(144, 2, 48, '2018-06-19 16:41:14.525'),
	(145, 2, 49, '2018-06-19 16:41:14.525'),
	(146, 2, 50, '2018-06-19 16:41:14.525'),
	(147, 2, 51, '2018-06-19 16:41:14.525'),
	(148, 2, 52, '2018-06-19 16:41:14.525'),
	(149, 2, 53, '2018-06-19 16:41:14.525'),
	(150, 2, 54, '2018-06-19 16:41:14.525'),
	(151, 2, 55, '2018-06-19 16:41:14.525'),
	(152, 2, 57, '2018-06-19 16:41:14.525'),
	(153, 2, 62, '2018-06-19 16:41:14.525'),
	(154, 2, 63, '2018-06-19 16:41:14.525'),
	(155, 2, 66, '2018-06-19 16:41:14.525'),
	(156, 2, 67, '2018-06-19 16:41:14.525'),
	(157, 2, 68, '2018-06-19 16:41:14.525'),
	(158, 2, 70, '2018-06-19 16:41:14.525'),
	(159, 2, 75, '2018-06-19 16:41:14.525'),
	(160, 2, 77, '2018-06-19 16:41:14.525'),
	(161, 2, 78, '2018-06-19 16:41:14.525'),
	(162, 2, 81, '2018-06-19 16:41:14.525'),
	(163, 2, 82, '2018-06-19 16:41:14.525'),
	(164, 2, 83, '2018-06-19 16:41:14.525'),
	(165, 2, 84, '2018-06-19 16:41:14.525'),
	(166, 2, 85, '2018-06-19 16:41:14.525'),
	(167, 2, 86, '2018-06-19 16:41:14.525'),
	(168, 2, 87, '2018-06-19 16:41:14.525'),
	(169, 2, 88, '2018-06-19 16:41:14.525'),
	(170, 2, 89, '2018-06-19 16:41:14.525'),
	(171, 2, 90, '2018-06-19 16:41:14.525'),
	(172, 2, 91, '2018-06-19 16:41:14.525'),
	(173, 2, 92, '2018-06-19 16:41:14.525'),
	(174, 2, 94, '2018-06-19 16:41:14.525'),
	(175, 2, 98, '2018-06-19 16:41:14.525'),
	(176, 2, 99, '2018-06-19 16:41:14.525'),
	(177, 2, 100, '2018-06-19 16:41:14.525'),
	(178, 2, 102, '2018-06-19 16:41:14.525'),
	(179, 2, 103, '2018-06-19 16:41:14.525'),
	(180, 2, 104, '2018-06-19 16:41:14.525'),
	(181, 2, 106, '2018-06-19 16:41:14.525'),
	(182, 2, 108, '2018-06-19 16:41:14.525'),
	(183, 2, 111, '2018-06-19 16:41:14.525'),
	(184, 2, 112, '2018-06-19 16:41:14.525'),
	(185, 2, 117, '2018-06-19 16:41:14.525'),
	(186, 2, 118, '2018-06-19 16:41:14.525');
/*!40000 ALTER TABLE `system_role_rule` ENABLE KEYS */;

-- 导出  表 mroot.system_rule 结构
CREATE TABLE IF NOT EXISTS `system_rule` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '权限id',
  `type` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '1-url地址,2-主菜单;3-子级菜单,4-按钮',
  `name` varchar(80) NOT NULL COMMENT '标识',
  `title` varchar(80) NOT NULL COMMENT '名称',
  `url` varchar(255) NOT NULL COMMENT 'url地址',
  `pid` bigint(20) unsigned NOT NULL COMMENT '父级权限',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '状态(1是正常，2是禁用，3是删除)',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `gmt_create` datetime(3) NOT NULL COMMENT '创建时间',
  `gmt_create_ip` varchar(64) NOT NULL COMMENT '创建IP',
  `gmt_modified` datetime(3) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_url` (`url`),
  KEY `idx_title` (`title`),
  KEY `idx_url` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8 COMMENT='权限表';

-- 正在导出表  mroot.system_rule 的数据：~0 rows (大约)
DELETE FROM `system_rule`;
/*!40000 ALTER TABLE `system_rule` DISABLE KEYS */;
INSERT INTO `system_rule` (`id`, `type`, `name`, `title`, `url`, `pid`, `sort`, `status`, `remark`, `gmt_create`, `gmt_create_ip`, `gmt_modified`) VALUES
	(1, 0, 'admin', '后台管理系统', '/', 0, 1, 1, '后台管理系统', '0000-00-00 00:00:00.000', '127.0.0.1', NULL),
	(2, 1, 'index', '首页', '/index', 1, 2, 1, '首页', '2017-11-29 00:24:47.000', '127.0.0.1', NULL),
	(3, 1, 'user', '用户', '/user', 1, 3, 1, '用户', '2017-11-29 00:25:48.000', '127.0.0.1', NULL),
	(4, 2, 'user', '用户管理', '/user/', 3, 4, 1, '用户管理', '2017-11-30 15:00:01.000', '127.0.0.1', NULL),
	(5, 3, 'user', '用户列表', '/user/index', 4, 5, 1, '用户列表', '2017-11-30 15:03:44.000', '127.0.0.1', NULL),
	(6, 4, 'user', '新增用户', '/user/add', 5, 6, 1, '新增用户', '2017-12-15 00:10:21.000', '127.0.0.1', NULL),
	(7, 4, 'user', '保存用户', '/user/save', 5, 7, 1, '保存用户', '2017-12-07 17:57:46.000', '127.0.0.1', NULL),
	(8, 4, 'user', '修改用户', '/user/edit', 5, 8, 1, '修改用户', '2018-01-02 19:02:47.000', '127.0.0.1', NULL),
	(9, 4, 'user', '更新用户', '/user/update', 5, 9, 1, '更新用户', '2018-01-02 19:03:37.000', '127.0.0.1', NULL),
	(10, 4, 'user', '删除用户', '/user/delete', 5, 10, 1, '删除用户', '2018-01-02 19:04:34.000', '127.0.0.1', NULL),
	(11, 4, 'user', '批量删除用户', '/user/deleteBatch', 5, 11, 1, '批量删除用户', '2018-01-08 23:21:19.000', '127.0.0.1', NULL),
	(12, 4, 'user', '查看用户', '/user/view', 5, 12, 1, '查看用户', '2018-01-18 00:13:58.000', '127.0.0.1', NULL),
	(13, 4, 'user', '修改昵称', '/user/nickName', 5, 13, 1, '修改昵称', '2018-01-18 00:15:15.000', '127.0.0.1', NULL),
	(14, 4, 'user', '保存昵称', '/user/updateNickName', 5, 14, 1, '保存昵称', '2018-01-18 00:16:17.000', '127.0.0.1', NULL),
	(15, 4, 'user', '修改密码', '/user/password', 5, 15, 1, '修改密码', '2018-01-21 00:46:41.000', '127.0.0.1', NULL),
	(16, 4, 'user', '保存密码', '/user/updatePassword', 5, 16, 1, '保存密码', '2018-02-24 15:46:46.000', '127.0.0.1', '2018-03-30 16:56:47.932'),
	(17, 4, 'user', '用户授权', '/user/authorization', 5, 17, 1, '用户授权', '2018-03-25 23:52:01.618', '127.0.0.1', '2018-04-03 21:08:19.761'),
	(18, 4, 'user', '保存用户授权', '/user/authorizationSave', 5, 18, 1, '保存用户授权', '2018-03-29 21:52:14.264', '127.0.0.1', '2018-03-29 21:52:29.533'),
	(19, 4, 'user', '用户回收站', '/user/recycleBin', 5, 19, 1, '用户回收站', '2018-03-29 21:54:32.434', '127.0.0.1', NULL),
	(20, 4, 'user', '恢复用户', '/user/recover', 5, 20, 1, '恢复用户', '2018-03-29 23:32:31.875', '127.0.0.1', NULL),
	(21, 4, 'user', '批量恢复用户', '/user/recoverBatch', 5, 21, 1, '批量恢复用户', '2018-03-29 23:33:31.626', '127.0.0.1', NULL),
	(22, 2, 'role', '角色管理', '/role', 3, 22, 1, '角色管理', '2018-02-24 15:49:16.000', '127.0.0.1', NULL),
	(23, 3, 'role', '角色列表', '/role/index', 22, 23, 1, '角色列表', '2018-03-05 13:41:46.000', '127.0.0.1', NULL),
	(24, 4, 'role', '新增角色', '/role/add', 23, 24, 1, '新增角色', '2018-03-05 14:12:34.000', '127.0.0.1', NULL),
	(25, 4, 'role', '保存角色', '/role/save', 23, 24, 1, '保存角色', '2018-03-05 14:13:22.000', '127.0.0.1', NULL),
	(26, 4, 'role', '编辑角色', '/role/edit', 23, 26, 1, '编辑角色', '2018-03-19 11:45:52.000', '127.0.0.1', NULL),
	(27, 4, 'role', '更新角色', '/role/update', 23, 27, 1, '更新角色', '2018-03-19 11:48:57.000', '127.0.0.1', NULL),
	(28, 4, 'role', '删除角色', '/role/delete', 23, 28, 1, '删除角色', '2018-03-19 11:50:19.000', '127.0.0.1', NULL),
	(29, 4, 'role', '批量删除角色', '/role/deleteBatch', 23, 29, 1, '批量删除角色', '2018-03-19 11:50:52.000', '127.0.0.1', NULL),
	(30, 4, 'role', '查看角色', '/role/view', 23, 30, 1, '查看角色', '2018-03-22 22:56:54.000', '127.0.0.1', NULL),
	(31, 4, 'role', '角色授权', '/role/authorization', 23, 31, 1, '角色授权', '2018-03-26 18:06:09.090', '127.0.0.1', '2018-03-26 19:37:36.395'),
	(32, 4, 'role', '保存角色授权', '/role/authorizationSave', 23, 32, 1, '保存角色授权', '2018-03-26 19:38:47.536', '127.0.0.1', NULL),
	(33, 4, 'role', '角色回收站', '/role/recycleBin', 23, 33, 1, '角色回收站', '2018-03-29 21:55:06.117', '127.0.0.1', NULL),
	(34, 4, 'role', '恢复角色', '/role/recover', 23, 34, 1, '恢复角色', '2018-03-30 00:55:12.814', '127.0.0.1', NULL),
	(35, 4, 'role', '批量恢复角色', '/role/recoverBatch', 23, 35, 1, '批量恢复角色', '2018-03-30 00:55:40.882', '127.0.0.1', NULL),
	(36, 3, 'rule', '权限列表', '/rule/index', 22, 36, 1, '权限列表', '2018-03-19 11:51:55.000', '127.0.0.1', NULL),
	(37, 4, 'rule', '新增权限', '/rule/add', 36, 37, 1, '新增权限', '2018-03-21 20:53:58.000', '127.0.0.1', NULL),
	(38, 4, 'rule', '保存权限', '/rule/save', 36, 38, 1, '保存权限', '2018-03-22 22:12:36.000', '127.0.0.1', NULL),
	(39, 4, 'rule', '编辑权限', '/rule/edit', 36, 39, 1, '编辑权限', '2018-03-22 22:13:59.000', '127.0.0.1', NULL),
	(40, 4, 'rule', '更新权限', '/rule/update', 36, 40, 1, '更新权限', '2018-03-22 22:31:32.000', '127.0.0.1', NULL),
	(41, 4, 'rule', '删除权限', '/rule/delete', 36, 41, 1, '删除权限', '2018-03-22 22:43:55.000', '127.0.0.1', NULL),
	(42, 4, 'rule', '批量删除权限', '/rule/deleteBatch', 36, 42, 1, '批量删除权限', '2018-03-22 22:44:22.000', '127.0.0.1', NULL),
	(43, 4, 'rule', '查看权限', '/rule/view', 36, 43, 1, '查看权限', '2018-03-22 22:44:52.000', '127.0.0.1', NULL),
	(44, 4, 'rule', '添加子级权限', '/rule/addChildren', 36, 44, 1, '添加子级权限', '2018-03-24 14:12:49.247', '127.0.0.1', '2018-03-26 16:31:27.306'),
	(45, 4, 'rule', '权限回收站', '/rule/recycleBin', 36, 45, 1, '权限回收站', '2018-03-29 21:55:32.965', '127.0.0.1', NULL),
	(46, 4, 'rule', '恢复权限', '/rule/recover', 36, 46, 1, '恢复权限', '2018-03-30 00:56:10.774', '127.0.0.1', NULL),
	(47, 4, 'rule', '批量恢复权限', '/rule/recoverBatch', 36, 47, 1, '批量恢复权限', '2018-03-30 00:56:30.825', '127.0.0.1', NULL),
	(48, 1, 'config', '系统', '/config', 1, 48, 1, '系统', '2018-03-22 22:45:40.000', '127.0.0.1', NULL),
	(49, 2, 'generateCode', '代码管理', '/generateCode', 48, 49, 1, '代码管理', '2018-03-22 22:46:07.000', '127.0.0.1', NULL),
	(50, 3, 'generateCode', '生成列表', '/generateCode/index', 49, 50, 1, '生成列表', '2018-03-22 22:46:31.000', '127.0.0.1', NULL),
	(51, 4, 'generateCode', '生成', '/generateCode/generate', 50, 51, 1, '生成', '2018-03-22 22:47:13.000', '127.0.0.1', NULL),
	(52, 4, 'generateCode', '保存生成', '/generateCode/save', 50, 52, 1, '保存生成', '2018-03-22 23:09:11.000', '127.0.0.1', '2018-03-26 16:29:39.421'),
	(53, 2, 'config', '系统配置管理', '/config/', 48, 53, 1, '系统配置管理', '2018-04-02 13:46:51.122', '127.0.0.1', '2018-04-02 16:36:30.300'),
	(54, 3, 'config', '系统配置列表', '/config/index', 53, 54, 1, '系统配置记录', '2018-04-02 16:32:57.043', '127.0.0.1', '2018-04-02 16:33:31.077'),
	(55, 4, 'config', '添加系统配置', '/config/add', 54, 55, 1, '添加系统配置', '2018-04-02 16:37:01.939', '127.0.0.1', NULL),
	(56, 4, 'config', '保存添加系统配置', '/config/save', 54, 56, 1, '保存添加系统配置', '2018-04-02 16:39:30.905', '127.0.0.1', NULL),
	(57, 4, 'config', '编辑系统配置', '/config/edit', 54, 57, 1, '编辑系统配置', '2018-04-02 16:41:29.905', '127.0.0.1', NULL),
	(58, 4, 'config', '删除系统配置', '/config/delete', 54, 58, 1, '删除系统配置', '2018-04-02 16:46:22.827', '127.0.0.1', NULL),
	(59, 4, 'config', '批量删除系统配置', '/config/deleteBatch', 54, 59, 1, '批量删除系统配置', '2018-04-02 16:46:47.753', '127.0.0.1', NULL),
	(60, 4, 'config', '查看系统配置', '/config/view', 54, 60, 1, '查看系统配置', '2018-04-02 16:47:04.869', '127.0.0.1', NULL),
	(61, 4, 'config', '更新编辑系统配置', '/config/update', 54, 61, 1, '更新编辑系统配置', '2018-04-02 16:48:37.511', '127.0.0.1', NULL),
	(62, 4, 'config', '回收站', '/config/recycleBin', 54, 62, 1, '回收站', '2018-04-02 16:49:03.246', '127.0.0.1', NULL),
	(63, 4, 'config', '恢复系统配置', '/config/recover', 54, 63, 1, '恢复系统配置', '2018-04-02 16:49:32.466', '127.0.0.1', NULL),
	(64, 4, 'config', '批量恢复系统配置', '/config/recoverBatch', 54, 64, 1, '批量恢复系统配置', '2018-04-02 16:49:54.702', '127.0.0.1', NULL),
	(65, 4, 'config', '清空缓存', '/config/clearCache', 54, 65, 1, '清空缓存', '2018-06-14 20:17:41.268', '192.168.64.1', NULL),
	(66, 2, 'scheduleJob', '定时任务管理', '/scheduleJob', 48, 66, 1, '定时任务管理', '2018-05-16 17:40:10.216', '127.0.0.1', '2018-05-17 14:29:15.007'),
	(67, 3, 'scheduleJob', '定时任务列表', '/scheduleJob/index', 66, 67, 1, '定时任务列表', '2018-05-16 17:40:58.497', '127.0.0.1', '2018-05-17 14:29:31.524'),
	(68, 4, 'scheduleJob', '新增定时任务', '/scheduleJob/add', 67, 68, 1, '新增定时任务', '2018-05-17 11:54:26.545', '127.0.0.1', '2018-05-17 14:29:48.124'),
	(69, 4, 'scheduleJob', '保存定时任务', '/scheduleJob/save', 67, 69, 1, '保存定时任务', '2018-05-17 11:55:00.337', '127.0.0.1', '2018-05-17 14:30:03.416'),
	(70, 4, 'scheduleJob', '编辑定时任务', '/scheduleJob/edit', 67, 70, 1, '编辑定时任务', '2018-05-17 14:22:34.185', '127.0.0.1', '2018-05-17 14:30:18.654'),
	(71, 4, 'scheduleJob', '更新定时任务', '/scheduleJob/update', 67, 71, 1, '更新定时任务', '2018-05-17 14:31:40.030', '127.0.0.1', NULL),
	(72, 4, 'scheduleJob', '运行', '/scheduleJob/run', 67, 72, 1, '运行定时任务', '2018-05-24 17:12:58.619', '127.0.0.1', NULL),
	(73, 4, 'scheduleJob', '暂停', '/scheduleJob/pause', 67, 73, 1, '暂停定时任务', '2018-05-24 17:13:55.358', '127.0.0.1', NULL),
	(74, 4, 'scheduleJob', '恢复', '/scheduleJob/resume', 67, 74, 1, '恢复定时任务', '2018-05-24 17:14:53.347', '127.0.0.1', NULL),
	(75, 4, 'scheduleJob', '查看定时任务', '/scheduleJob/view', 67, 75, 1, '查看定时任务', '2018-05-28 10:53:21.545', '127.0.0.1', NULL),
	(76, 4, 'scheduleJob', '删除定时任务', '/scheduleJob/delete', 67, 76, 1, '删除定时任务', '2018-06-01 15:40:32.555', '127.0.0.1', NULL),
	(77, 3, 'scheduleJobLog', '定时任务日志列表', '/scheduleJobLog/index', 66, 77, 1, '定时任务日志列表', '2018-05-25 17:43:50.600', '127.0.0.1', NULL),
	(78, 4, 'scheduleJobLog', '查看定时任务日志', '/scheduleJobLog/view', 77, 78, 1, '查看定时任务日志', '2018-05-25 17:44:42.301', '127.0.0.1', NULL),
	(79, 4, 'scheduleJobLog', '删除定时任务日志', '/scheduleJobLog/delete', 77, 79, 1, '删除定时任务日志', '2018-05-25 17:45:02.113', '127.0.0.1', '2018-06-01 15:37:23.629'),
	(80, 4, 'scheduleJobLog', '批量删除定时任务日志', '/scheduleJobLog/deleteBatch', 77, 80, 1, '批量删除定时任务日志', '2018-05-25 17:45:42.938', '127.0.0.1', '2018-06-01 15:37:37.408'),
	(81, 1, 'log', '记录管理', '/log/index', 48, 81, 1, '记录管理', '2018-03-30 01:37:59.601', '127.0.0.1', '2018-03-30 10:57:54.611'),
	(82, 2, 'requestLog', '请求记录管理', '/requestLog/index', 81, 82, 1, '请求记录管理', '2018-03-30 10:57:21.936', '127.0.0.1', '2018-04-02 16:35:47.329'),
	(83, 4, 'requestLog', '查看请求记录', '/requestLog/view', 82, 83, 1, '查看请求记录', '2018-03-30 14:39:13.945', '127.0.0.1', '2018-04-01 01:27:35.730'),
	(84, 4, 'requestLog', '删除请求记录', '/requestLog/delete', 82, 84, 1, '删除请求记录', '2018-04-01 00:22:47.545', '127.0.0.1', NULL),
	(85, 4, 'requestLog', '批量删除请求记录', '/requestLog/deleteBatch', 82, 85, 1, '批量删除请求记录', '2018-04-01 00:23:32.373', '127.0.0.1', NULL),
	(86, 4, 'requestLog', '清空请求记录', '/requestLog/truncate', 82, 86, 1, '清空请求记录', '2018-04-01 01:27:58.228', '127.0.0.1', '2018-04-01 01:28:17.588'),
	(87, 2, 'loggingEvent', '日志记录管理', '/loggingEvent/index', 81, 87, 1, '日志记录管理', '2018-04-02 12:13:03.255', '127.0.0.1', '2018-04-02 16:36:14.272'),
	(88, 4, 'loggingEvent', '查看日志', '/loggingEvent/view', 87, 88, 1, '日志列表', '2018-04-02 13:46:21.930', '127.0.0.1', '2018-04-02 14:46:34.977'),
	(89, 1, 'cms', '内容管理', '/cms', 1, 89, 1, '内容', '2018-04-03 23:54:09.753', '127.0.0.1', '2018-04-25 17:09:05.683'),
	(90, 2, 'category', '文章分类管理', '/category', 89, 90, 1, '文章分类管理', '2018-04-07 23:30:22.693', '127.0.0.1', '2018-04-25 17:11:29.796'),
	(91, 3, 'category', '文章分类列表', '/category/index', 90, 91, 1, '文章分类列表', '2018-04-25 17:12:22.783', '127.0.0.1', NULL),
	(92, 4, 'category', '添加文章分类', '/category/add', 91, 92, 1, '添加文章分类', '2018-04-25 17:12:58.888', '127.0.0.1', NULL),
	(93, 4, 'category', '保存文章分类', '/category/save', 91, 93, 1, '保存文章分类', '2018-04-25 17:42:53.402', '127.0.0.1', NULL),
	(94, 4, 'category', '编辑文章分类', '/category/edit', 91, 94, 1, '编辑文章分类', '2018-04-26 17:39:58.511', '127.0.0.1', NULL),
	(95, 4, 'category', '更新文章分类', '/category/update', 91, 95, 1, '更新文章分类', '2018-04-26 17:40:34.142', '127.0.0.1', NULL),
	(96, 4, 'category', '删除文章分类', '/category/delete', 91, 96, 1, '删除文章分类', '2018-04-26 17:40:54.731', '127.0.0.1', NULL),
	(97, 4, 'category', '批量删除文章分类', '/category/deleteBatch', 91, 97, 1, '批量删除文章分类', '2018-04-26 17:41:18.690', '127.0.0.1', '2018-04-26 17:43:22.820'),
	(98, 4, 'category', '查看文章分类', '/category/view', 91, 98, 1, '查看文章分类', '2018-04-26 17:41:39.847', '127.0.0.1', NULL),
	(99, 4, 'category', '文章分类回收站', '/category/recycleBin', 91, 99, 1, '文章分类回收站', '2018-04-26 17:42:13.870', '127.0.0.1', NULL),
	(100, 4, 'category', '恢复文章分类', '/category/recover', 91, 100, 1, '恢复文章分类', '2018-04-26 17:42:38.745', '127.0.0.1', NULL),
	(101, 4, 'category', '批量恢复文章分类', '/category/recoverBatch', 91, 101, 1, '批量恢复文章分类', '2018-04-26 17:43:06.213', '127.0.0.1', '2018-04-27 17:46:28.607'),
	(102, 2, 'article', '文章管理', '/article', 89, 102, 1, '文章管理', '2018-04-28 11:57:12.133', '127.0.0.1', '2018-04-28 14:56:15.577'),
	(103, 3, 'article', '文章列表', '/article/index', 102, 103, 1, '文章列表', '2018-04-28 11:57:52.684', '127.0.0.1', '2018-04-28 14:56:48.736'),
	(104, 4, 'article', '新增文章', '/article/add', 103, 104, 1, '新增文章', '2018-04-28 11:58:12.402', '127.0.0.1', '2018-04-28 14:57:14.050'),
	(105, 4, 'article', '保存文章', '/article/save', 103, 105, 1, '保存文章', '2018-04-28 14:59:43.012', '127.0.0.1', '2018-04-28 17:37:25.858'),
	(106, 4, 'article', '编辑文章', '/article/edit', 103, 106, 1, '编辑文章', '2018-04-28 15:00:15.952', '127.0.0.1', NULL),
	(107, 4, 'article', '更新文章', '/article/update', 103, 107, 1, '更新文章', '2018-04-28 15:00:38.687', '127.0.0.1', NULL),
	(108, 4, 'article', '查看文章', '/article/view', 103, 108, 1, '查看文章', '2018-04-28 15:01:52.515', '127.0.0.1', NULL),
	(109, 4, 'article', '删除文章', '/article/delete', 103, 109, 1, '删除文章', '2018-04-28 15:05:02.230', '127.0.0.1', NULL),
	(110, 4, 'article', '批量删除文章', '/article/deleteBatch', 103, 110, 1, '批量删除文章', '2018-04-28 15:05:54.868', '127.0.0.1', NULL),
	(111, 4, 'article', '文章回收站', '/article/recycleBin', 103, 111, 1, '文章回收站', '2018-04-28 15:10:00.127', '127.0.0.1', NULL),
	(112, 4, 'article', '恢复文章', '/article/recover', 103, 112, 1, '恢复文章', '2018-04-28 15:10:51.437', '127.0.0.1', NULL),
	(113, 4, 'article', '批量恢复文章', '/article/recoverBatch', 103, 113, 1, '批量恢复文章', '2018-04-28 15:12:16.910', '127.0.0.1', '2018-05-17 14:28:42.050'),
	(114, 4, 'article', '上传文章封面', '/article/uploadCover', 103, 114, 1, '上传文章封面', '2018-05-07 15:23:25.899', '127.0.0.1', '2018-05-17 14:29:01.376'),
	(115, 4, 'ueditor', '百度编辑器', '/ueditor', 103, 120, 1, '百度编辑器', '2018-06-08 11:47:01.258', '192.168.64.1', '2018-06-08 11:55:17.524'),
	(116, 4, 'ueditor', '百度编辑器上传', '/ueditor/upload', 103, 121, 1, '百度编辑器上传', '2018-06-08 11:49:30.711', '192.168.64.1', '2018-06-08 11:55:52.579'),
	(117, 3, 'blog', '博客列表', '/blog/index', 102, 117, 1, '博客列表', '2018-05-28 17:20:49.825', '127.0.0.1', NULL),
	(118, 4, 'blog', '查看博客', '/blog/view', 117, 118, 1, '查看博客', '2018-05-30 11:51:40.975', '127.0.0.1', NULL);
/*!40000 ALTER TABLE `system_rule` ENABLE KEYS */;

-- 导出  表 mroot.system_schedule_job 结构
CREATE TABLE IF NOT EXISTS `system_schedule_job` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `name` varchar(80) NOT NULL COMMENT '标识',
  `title` varchar(80) NOT NULL COMMENT '名称',
  `bean_name` varchar(100) NOT NULL COMMENT 'Spring Bean名称',
  `method_name` varchar(100) NOT NULL COMMENT '方法名',
  `params` varchar(255) DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) NOT NULL COMMENT 'cron表达式',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '状态(1是正常，2是禁用，3是删除)',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `gmt_create` datetime(3) NOT NULL COMMENT '创建时间',
  `gmt_create_ip` varchar(64) NOT NULL COMMENT '创建IP',
  `gmt_modified` datetime(3) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='定时任务';

-- 正在导出表  mroot.system_schedule_job 的数据：~0 rows (大约)
DELETE FROM `system_schedule_job`;
/*!40000 ALTER TABLE `system_schedule_job` DISABLE KEYS */;
INSERT INTO `system_schedule_job` (`id`, `name`, `title`, `bean_name`, `method_name`, `params`, `cron_expression`, `sort`, `status`, `remark`, `gmt_create`, `gmt_create_ip`, `gmt_modified`) VALUES
	(1, 'JOB01', '删除后台请求记录', 'requestLogJob', 'removeSize2', NULL, '0 0 0/1 * * ? *', 1, 1, '一次删除2条后台请求记录', '2018-05-17 14:09:13.872', '127.0.0.1', '2018-06-12 17:30:41.621');
/*!40000 ALTER TABLE `system_schedule_job` ENABLE KEYS */;

-- 导出  表 mroot.system_schedule_job_log 结构
CREATE TABLE IF NOT EXISTS `system_schedule_job_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `schedule_job_id` bigint(20) unsigned NOT NULL COMMENT '定时任务ID',
  `title` varchar(80) NOT NULL COMMENT '名称',
  `bean_name` varchar(100) NOT NULL COMMENT 'Spring Bean名称',
  `method_name` varchar(100) NOT NULL COMMENT '方法名',
  `params` varchar(255) DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) NOT NULL COMMENT 'Cron表达式',
  `error_message` varchar(2000) DEFAULT NULL COMMENT '失败信息',
  `times` bigint(20) unsigned NOT NULL COMMENT '耗时(单位：毫秒)',
  `execution_result` varchar(4) NOT NULL COMMENT '执行结果(成功，失败)',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '状态(1是正常，2是禁用，3是删除)',
  `gmt_create` datetime(3) NOT NULL COMMENT '创建时间',
  `gmt_create_ip` varchar(64) NOT NULL COMMENT '创建IP',
  `gmt_modified` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时任务记录';

-- 正在导出表  mroot.system_schedule_job_log 的数据：~0 rows (大约)
DELETE FROM `system_schedule_job_log`;
/*!40000 ALTER TABLE `system_schedule_job_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_schedule_job_log` ENABLE KEYS */;

-- 导出  表 mroot.system_user 结构
CREATE TABLE IF NOT EXISTS `system_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `type` tinyint(4) unsigned NOT NULL DEFAULT '2' COMMENT '用户类型(1是管理员，2是普通用户)',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `nick_name` varchar(50) DEFAULT NULL COMMENT '昵称',
  `password` varchar(128) NOT NULL COMMENT '密码',
  `real_name` varchar(96) DEFAULT NULL COMMENT '真实姓名',
  `phone` varchar(32) DEFAULT NULL COMMENT '手机号码',
  `email` varchar(128) DEFAULT NULL COMMENT '电子邮箱',
  `gmt_create` datetime(3) NOT NULL COMMENT '创建时间',
  `gmt_create_ip` varchar(50) NOT NULL COMMENT '创建IP',
  `gmt_modified` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '状态(1是正常，2是禁用，3是删除)',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_username` (`username`),
  UNIQUE KEY `uk_nick_name` (`nick_name`),
  UNIQUE KEY `uk_phone` (`phone`),
  UNIQUE KEY `uk_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- 正在导出表  mroot.system_user 的数据：~0 rows (大约)
DELETE FROM `system_user`;
/*!40000 ALTER TABLE `system_user` DISABLE KEYS */;
INSERT INTO `system_user` (`id`, `type`, `username`, `nick_name`, `password`, `real_name`, `phone`, `email`, `gmt_create`, `gmt_create_ip`, `gmt_modified`, `status`, `avatar`) VALUES
	(1, 1, 'admin', '超级管理员', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', 'e0c25b1be57745aa3b82a6228a7f734e', '46061ddb5d6516b6369ae0faa922853d', 'ab125c1b067fa2554f6bace8f189074e0b97692ef52329862c863fea9d66b7db', '0000-00-00 00:00:00.000', '192.168.10.77', '2018-06-15 17:04:39.970', 1, '/data/images/avatar/MQ.png'),
	(2, 1, 'administrator', 'administrator', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-01-04 18:01:50.000', '127.0.0.1', '2018-04-14 17:09:23.806', 1, NULL),
	(3, 1, 'controller', 'controller', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-01-04 18:02:14.000', '127.0.0.1', '2018-04-14 17:05:25.569', 1, NULL),
	(4, 1, 'root', 'root', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-01-04 18:02:45.000', '127.0.0.1', '2018-04-14 17:04:08.754', 1, NULL),
	(5, 1, 'member', 'member', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-01-04 18:03:04.000', '127.0.0.1', '2018-04-14 17:03:18.658', 1, NULL),
	(6, 1, 'manager', 'manager', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-01-04 18:03:33.000', '127.0.0.1', '2018-04-14 17:02:50.970', 1, NULL),
	(7, 1, 'test', 'test', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-01-04 18:03:49.000', '127.0.0.1', '2018-04-14 17:01:56.513', 1, NULL),
	(8, 1, 'ceshi', 'ceshi', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-01-04 18:04:04.000', '127.0.0.1', '2018-04-14 17:01:15.052', 1, NULL),
	(9, 1, '123456', '123456', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-01-04 18:04:22.000', '127.0.0.1', '2018-04-14 17:01:05.450', 1, NULL),
	(10, 1, 'user', 'user', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-03-30 16:02:23.000', '127.0.0.1', '2018-04-14 16:56:36.797', 1, NULL),
	(11, 1, 'fsdafasdf', 'fasdfasdf', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-03-31 22:30:42.135', '127.0.0.1', '2018-06-09 15:50:03.752', 1, NULL),
	(12, 2, 'fasdfasdffasdfa', 'fasdfasdffasdfa', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-04-03 23:45:50.446', '127.0.0.1', '2018-04-14 16:55:50.141', 1, NULL),
	(13, 2, 'fasdfasdffdsaf', 'fasdfasdffdsaf', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-04-03 23:47:05.167', '127.0.0.1', '2018-04-14 16:55:42.181', 1, NULL),
	(14, 2, 'ffffff', 'ffffff', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-04-03 23:52:29.044', '127.0.0.1', '2018-04-14 16:55:26.769', 1, NULL),
	(15, 2, 'ffffff2', 'ffffff2', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-04-03 23:58:35.950', '127.0.0.1', '2018-04-14 16:53:34.143', 1, NULL),
	(16, 2, 'ffffffff', 'ffffffff', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-04-04 00:05:05.155', '127.0.0.1', '2018-04-14 16:38:58.952', 1, NULL),
	(17, 2, 'ffffffffffff', 'ffffffffffff', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-04-04 00:06:50.203', '127.0.0.1', '2018-04-14 16:38:44.985', 1, NULL),
	(18, 2, 'ffffffffffff2', 'ffffffffffff2', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-04-04 00:06:59.562', '127.0.0.1', '2018-04-14 16:37:10.225', 1, NULL),
	(19, 2, 'fsdfasdfasf23', 'fsdfasdfasf23', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-04-04 00:09:46.064', '127.0.0.1', '2018-04-14 16:36:59.605', 1, NULL),
	(20, 2, 'fasdf23234', 'fasdf23234', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-04-04 00:09:51.256', '127.0.0.1', '2018-04-14 16:33:53.966', 1, NULL),
	(21, 2, '123456vzxcv', '123456vzxcv', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-04-06 01:41:57.846', '127.0.0.1', '2018-04-14 16:28:27.164', 1, NULL),
	(22, 2, '123456vzxcv2', '123456vzxcv2', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-04-06 01:42:14.892', '127.0.0.1', '2018-04-14 16:28:11.198', 1, NULL),
	(23, 2, 'fadsfasf', 'fadsfasf', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-04-06 01:43:45.955', '127.0.0.1', '2018-04-17 15:46:34.873', 1, NULL),
	(24, 2, 'fsdafasdfs', 'fsdafasdfs', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-04-06 01:44:06.878', '127.0.0.1', '2018-04-17 13:42:57.391', 3, NULL),
	(25, 2, 'fdsfasdf', 'fdsfasdf', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-04-06 01:44:27.762', '127.0.0.1', '2018-04-17 13:42:57.391', 3, NULL),
	(26, 2, 'sdafasdfasd', 'sdafasdfasd', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-04-06 01:45:31.526', '127.0.0.1', '2018-04-17 13:42:57.391', 3, NULL),
	(27, 2, 'fasdfasdfasdfasdf', 'fasdfasdfasdfasdf1', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-04-07 23:29:55.218', '127.0.0.1', '2018-06-09 15:46:36.618', 3, NULL),
	(28, 2, '复古风大概多少法规5435z', '复古风大概多少法规5435z', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-04-23 16:18:23.382', '127.0.0.1', '2018-06-09 15:45:17.202', 1, NULL),
	(29, 2, 'fsdaf235sdaf234', 'fsdaf235sdaf234', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', NULL, NULL, NULL, '2018-04-27 17:36:11.074', '127.0.0.1', '2018-04-27 17:41:14.787', 1, NULL),
	(30, 2, 'fadsfasdf', 'fadsfasdf', 'c9a4d1ebd671576eda61d91bf30a5aea26e1fd179a06dd6e3d843d11c7d59d3a93c6131225dd9376702f447e680a436ab495da31fe76e34a9c956205e79e2350', '', NULL, NULL, '2018-06-01 16:11:00.194', '127.0.0.1', '2018-06-01 16:21:08.069', 1, NULL);
/*!40000 ALTER TABLE `system_user` ENABLE KEYS */;

-- 导出  表 mroot.system_user_role 结构
CREATE TABLE IF NOT EXISTS `system_user_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id编号',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `role_id` bigint(20) unsigned NOT NULL COMMENT '角色id',
  `gmt_create` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_userId` (`user_id`),
  KEY `idx_roleId` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='用户对应角色的权限表';

-- 正在导出表  mroot.system_user_role 的数据：~0 rows (大约)
DELETE FROM `system_user_role`;
/*!40000 ALTER TABLE `system_user_role` DISABLE KEYS */;
INSERT INTO `system_user_role` (`id`, `user_id`, `role_id`, `gmt_create`) VALUES
	(1, 1, 1, '2018-02-26 19:16:52.000'),
	(2, 9, 2, '2018-03-30 16:05:06.939'),
	(9, 11, 2, '2018-06-09 15:52:44.448');
/*!40000 ALTER TABLE `system_user_role` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
