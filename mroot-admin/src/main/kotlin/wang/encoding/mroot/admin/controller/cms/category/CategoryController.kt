/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.cms.category


import com.alibaba.fastjson.JSONObject
import com.baomidou.mybatisplus.plugins.Page
import org.apache.shiro.authz.annotation.RequiresPermissions
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import wang.encoding.mroot.admin.common.controller.BaseAdminController
import wang.encoding.mroot.model.entity.cms.Category
import wang.encoding.mroot.model.enums.StatusEnum
import wang.encoding.mroot.service.cms.CategoryService
import java.math.BigInteger
import javax.servlet.http.HttpServletRequest
import wang.encoding.mroot.common.annotation.FormToken
import wang.encoding.mroot.common.annotation.RequestLogAnnotation
import wang.encoding.mroot.common.constant.RequestLogConstant
import wang.encoding.mroot.common.business.ResultData
import wang.encoding.mroot.common.exception.ControllerException
import wang.encoding.mroot.model.entity.cms.Article
import wang.encoding.mroot.model.enums.BooleEnum
import wang.encoding.mroot.service.cms.ArticleService


/**
 * 后台 文章分类 控制器
 *
 *@author ErYang
 */
@RestController
@RequestMapping(value = ["/category"])
class CategoryController : BaseAdminController() {


    @Autowired
    private lateinit var categoryService: CategoryService


    @Autowired
    private lateinit var articleService: ArticleService


    companion object {

        /**
         * logger
         */
        private val logger: Logger = LoggerFactory.getLogger(CategoryController::class.java)

        /**
         * 模块
         */
        private const val MODULE_NAME: String = "/category"
        /**
         * 视图目录
         */
        private const val VIEW_PATH: String = "/cms/category"
        /**
         * 对象名称
         */
        private const val VIEW_MODEL_NAME: String = "category"
        /**
         * 首页
         */
        private const val INDEX: String = "/index"
        private const val INDEX_URL: String = MODULE_NAME + INDEX
        private const val INDEX_VIEW: String = VIEW_PATH + INDEX
        /**
         * 新增
         */
        private const val ADD: String = "/add"
        private const val ADD_URL: String = MODULE_NAME + ADD
        private const val ADD_VIEW: String = VIEW_PATH + ADD
        /**
         * 保存
         */
        private const val SAVE: String = "/save"
        private const val SAVE_URL: String = MODULE_NAME + SAVE
        /**
         * 修改
         */
        private const val EDIT: String = "/edit"
        private const val EDIT_URL: String = MODULE_NAME + EDIT
        private const val EDIT_VIEW: String = VIEW_PATH + EDIT
        /**
         * 更新
         */
        private const val UPDATE: String = "/update"
        private const val UPDATE_URL: String = MODULE_NAME + UPDATE
        /**
         * 查看
         */
        private const val VIEW: String = "/view"
        private const val VIEW_URL: String = MODULE_NAME + VIEW
        private const val VIEW_VIEW: String = VIEW_PATH + VIEW
        /**
         * 删除
         */
        private const val DELETE: String = "/delete"
        private const val DELETE_URL: String = MODULE_NAME + DELETE
        private const val DELETE_BATCH: String = "/deleteBatch"
        private const val DELETE_BATCH_URL: String = MODULE_NAME + DELETE_BATCH

        /**
         * 回收站
         */
        private const val RECYCLE_BIN_INDEX: String = "/recycleBin"
        private const val RECYCLE_BIN_INDEX_URL: String = MODULE_NAME + RECYCLE_BIN_INDEX
        private const val RECYCLE_BIN_INDEX_VIEW: String = VIEW_PATH + RECYCLE_BIN_INDEX
        /**
         * 恢复
         */
        private const val RECOVER: String = "/recover"
        private const val RECOVER_URL: String = MODULE_NAME + RECOVER

        private const val RECOVER_BATCH: String = "/recoverBatch"
        private const val RECOVER_BATCH_URL: String = MODULE_NAME + RECOVER_BATCH

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 列表页面
     *
     * @param request HttpServletRequest
     * @return ModelAndView
     */
    @RequiresPermissions(value = [INDEX_URL])
    @RequestMapping(INDEX)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_CATEGORY_INDEX)
    @Throws(ControllerException::class)
    fun index(request: HttpServletRequest): ModelAndView {
        val modelAndView = ModelAndView(super.initView(INDEX_VIEW))
        super.initViewTitleAndModelUrl(INDEX_URL, MODULE_NAME, request)

        val category = Category()
        val title: String? = request.getParameter("title")
        if (null != title && title.isNotBlank()) {
            category.title = title
            modelAndView.addObject("title", title)
        }
        category.status = StatusEnum.NORMAL.key
        val page: Page<Category> = categoryService.list2page(super.initPage(request), category, Category.ID, false)!!
        modelAndView.addObject(VIEW_PAGE_NAME, page)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 添加页面
     *
     * @param request HttpServletRequest
     * @return ModelAndView
     */
    @RequiresPermissions(value = [ADD_URL])
    @RequestMapping(ADD)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_CATEGORY_ADD)
    @FormToken(init = true)
    @Throws(ControllerException::class)
    fun add(request: HttpServletRequest): ModelAndView {
        val modelAndView = ModelAndView(super.initView(ADD_VIEW))
        super.initViewTitleAndModelUrl(ADD_URL, MODULE_NAME, request)

        // 最大排序值
        val maxSort: Int = categoryService.getMax2Sort() + 1
        modelAndView.addObject("maxSort", maxSort)

        this.getCategory2Tree(request)

        // 设置上个请求地址
        super.initRefererUrl(request)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理保存
     *
     * @param request HttpServletRequest
     * @param category Category
     * @return ModelAndView
     */
    @RequiresPermissions(value = [SAVE_URL])
    @RequestMapping(SAVE)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_CATEGORY_SAVE)
    @FormToken(remove = true)
    @Throws(ControllerException::class)
    fun save(request: HttpServletRequest, category: Category): Any {

        if (httpRequestUtil.isAjaxRequest(request)) {
            val failResult: ResultData = ResultData.fail()

            // 创建 Category 对象
            val saveCategory: Category = this.initAddData(request, category)

            // 发布的文章是否需要审核
            val auditStr: String? = request.getParameter("auditStr")
            when {
                auditStr.equals(configProperties.bootstrapSwitchEnabled) -> saveCategory.audit = BooleEnum.YES.key
                null == auditStr -> saveCategory.audit = BooleEnum.NO.key
                else -> saveCategory.audit = BooleEnum.NO.key
            }
            // 是否允许发布内容
            val allowStr: String? = request.getParameter("allowStr")
            when {
                allowStr.equals(configProperties.bootstrapSwitchEnabled) -> saveCategory.allow = BooleEnum.YES.key
                null == allowStr -> saveCategory.allow = BooleEnum.NO.key
                else -> saveCategory.allow = BooleEnum.NO.key
            }
            // 是否前台可见
            val showStr: String? = request.getParameter("showStr")
            when {
                showStr.equals(configProperties.bootstrapSwitchEnabled) -> saveCategory.show = BooleEnum.YES.key
                null == showStr -> saveCategory.show = BooleEnum.NO.key
                else -> saveCategory.show = BooleEnum.NO.key
            }

            // Hibernate Validation  验证数据
            val validationResult: String? = categoryService.validationCategory(saveCategory)
            if (null != validationResult && validationResult.isNotBlank()) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult)
            }

            // 验证数据唯一性
            val flag: Boolean = this.validationAddData(saveCategory, failResult)
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult)
            }

            // 新增用户 id
            val id: BigInteger? = categoryService.saveBackId(saveCategory)
            return super.initSaveJSONObject(id)
        } else {
            return super.initErrorRedirectUrl()
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 编辑页面
     *
     * @param id String
     * @param request HttpServletRequest
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(value = [EDIT_URL])
    @RequestMapping("$EDIT/{id}")
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_CATEGORY_EDIT)
    @FormToken(init = true)
    @Throws(ControllerException::class)
    fun edit(@PathVariable(ID_NAME) id: String, request: HttpServletRequest,
             redirectAttributes: RedirectAttributes): ModelAndView {
        val modelAndView = ModelAndView(super.initView(EDIT_VIEW))


        super.initViewTitleAndModelUrl(EDIT_URL, MODULE_NAME, request)
        val idValue: BigInteger? = super.getId(id)
        if (null == idValue || BigInteger.ZERO > idValue) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        }
        // 数据真实性
        val category: Category? = categoryService.getById(idValue)
                ?: return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        if (StatusEnum.DELETE.key == category!!.status) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        }
        this.getCategory2Tree(request)

        modelAndView.addObject(VIEW_MODEL_NAME, category)
        // 设置上个请求地址
        super.initRefererUrl(request)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理更新
     *
     * @param request HttpServletRequest
     * @param category Category
     * @return ModelAndView
     */
    @RequiresPermissions(value = [UPDATE_URL])
    @RequestMapping(UPDATE)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_CATEGORY_UPDATE)
    @FormToken(remove = true)
    @Throws(ControllerException::class)
    fun update(request: HttpServletRequest, category: Category): Any {
        if (httpRequestUtil.isAjaxRequest(request)) {
            val failResult: ResultData = ResultData.fail()
            // 验证数据
            val idValue: BigInteger? = super.getId(request)
            if (null == idValue || BigInteger.ZERO > idValue) {
                return super.initErrorCheckJSONObject(failResult)
            }
            // 数据真实性
            val categoryBefore: Category = categoryService.getById(idValue)
                    ?: return super.initErrorCheckJSONObject(failResult)
            if (StatusEnum.DELETE.key == categoryBefore.status) {
                return super.initErrorCheckJSONObject(failResult)
            }

            // 创建 Category 对象
            var editCategory: Category = Category.copy2New(category)
            editCategory.id = categoryBefore.id
            val statusStr: String? = super.getStatusStr(request)
            when {
                statusStr.equals(configProperties.bootstrapSwitchEnabled) -> editCategory.status = StatusEnum.NORMAL.key
                null == statusStr -> editCategory.status = StatusEnum.DISABLE.key
                else -> editCategory.status = StatusEnum.DISABLE.key
            }

            // 发布的文章是否需要审核
            val auditStr: String? = request.getParameter("auditStr")
            when {
                auditStr.equals(configProperties.bootstrapSwitchEnabled) -> editCategory.audit = BooleEnum.YES.key
                null == auditStr -> editCategory.audit = BooleEnum.NO.key
                else -> editCategory.audit = BooleEnum.NO.key
            }
            // 是否允许发布内容
            val allowStr: String? = request.getParameter("allowStr")
            when {
                allowStr.equals(configProperties.bootstrapSwitchEnabled) -> editCategory.allow = BooleEnum.YES.key
                null == allowStr -> editCategory.allow = BooleEnum.NO.key
                else -> editCategory.allow = BooleEnum.NO.key
            }
            // 是否前台可见
            val showStr: String? = request.getParameter("showStr")
            when {
                showStr.equals(configProperties.bootstrapSwitchEnabled) -> editCategory.show = BooleEnum.YES.key
                null == showStr -> editCategory.show = BooleEnum.NO.key
                else -> editCategory.show = BooleEnum.NO.key
            }
            editCategory = this.initEditData(editCategory)

            // Hibernate Validation 验证数据
            val validationResult: String? = categoryService.validationCategory(editCategory)
            if (null != validationResult && validationResult.isNotBlank()) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult)
            }

            // 验证数据唯一性
            val flag: Boolean = this.validationEditData(category,
                    categoryBefore, failResult)
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult)
            }

            // 修改 id
            val id: BigInteger? = categoryService.updateBackId(editCategory)
            return super.initUpdateJSONObject(id)
        } else {
            return super.initErrorRedirectUrl()
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查看页面
     *
     * @param id String
     * @param request HttpServletRequest
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(value = [VIEW_URL])
    @RequestMapping("$VIEW/{id}")
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_CATEGORY_VIEW)
    @Throws(ControllerException::class)
    fun view(@PathVariable(ID_NAME) id: String, request: HttpServletRequest,
             redirectAttributes: RedirectAttributes): ModelAndView {
        val modelAndView = ModelAndView(super.initView(VIEW_VIEW))
        super.initViewTitleAndModelUrl(VIEW_URL, MODULE_NAME, request)

        val idValue: BigInteger? = super.getId(id)
        // 验证数据
        if (null == idValue || BigInteger.ZERO > idValue) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        }
        // 数据真实性
        val category: Category? = categoryService.getById(idValue)
                ?: return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        if (null != category) {
            modelAndView.addObject(VIEW_MODEL_NAME, category)
        }

        // 设置上个请求地址
        super.initRefererUrl(request)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除
     * @param request HttpServletRequest
     * @param id String
     * @return JSONObject
     */
    @RequiresPermissions(value = [DELETE_URL])
    @RequestMapping("$DELETE/{id}")
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_CATEGORY_DELETE)
    @Throws(ControllerException::class)
    fun delete(@PathVariable(ID_NAME) id: String, request: HttpServletRequest): JSONObject? {
        if (super.isAjaxRequest(request)) {
            val idValue: BigInteger? = super.getId(id)
            // 验证数据
            if (null == idValue || BigInteger.ZERO > idValue) {
                super.initErrorCheckJSONObject()
            }
            val category: Category? = categoryService.getById(idValue!!)
            if (null == category) {
                super.initErrorCheckJSONObject()
            }
            val articleList: List<Article>? = articleService.listByCategoryId(category!!.id!!)
            if (null != articleList && articleList.isNotEmpty()) {
                val failResult: ResultData = ResultData.fail()
                return super.initErrorJSONObject(failResult, "message.cms.category.delete.error")
            }
            // 删除 Category id
            val backId: BigInteger? = categoryService.removeBackId(category.id!!)
            return super.initDeleteJSONObject(backId)
        }
        return super.initReturnErrorJSONObject()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除
     * @param request HttpServletRequest
     * @return JSONObject
     */
    @RequiresPermissions(value = [DELETE_BATCH_URL])
    @RequestMapping(DELETE_BATCH)
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_CATEGORY_DELETE_BATCH)
    @Throws(ControllerException::class)
    fun deleteBatch(request: HttpServletRequest): JSONObject? {
        if (super.isAjaxRequest(request)) {
            val idArray: ArrayList
            <BigInteger>? = super.getIdArray(request)
            // 验证数据
            if (null == idArray || idArray.isEmpty()) {
                super.initErrorCheckJSONObject()
            }
            val articleList: List<Article>? = articleService.listByCategoryIdArray(idArray)
            if (null != articleList && articleList.isNotEmpty()) {
                val failResult: ResultData = ResultData.fail()
                return super.initErrorJSONObject(failResult, "message.cms.category.delete.error")
            }
            var flag = false
            if (null != idArray) {
                flag = categoryService.removeBatch2UpdateStatus(idArray)!!
            }
            return if (flag) {
                super.initDeleteJSONObject(BigInteger.ONE)
            } else {
                super.initDeleteJSONObject(BigInteger.ZERO)
            }
        }
        return super.initReturnErrorJSONObject()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 回收站页面
     *
     * @param request HttpServletRequest
     * @return ModelAndView
     */
    @RequiresPermissions(value = [RECYCLE_BIN_INDEX_URL])
    @RequestMapping(RECYCLE_BIN_INDEX)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_CATEGORY_RECYCLE_BIN_INDEX)
    @Throws(ControllerException::class)
    fun recycleBin(request: HttpServletRequest): ModelAndView {
        val modelAndView = ModelAndView(super.initView(RECYCLE_BIN_INDEX_VIEW))
        super.initViewTitleAndModelUrl(RECYCLE_BIN_INDEX_URL, MODULE_NAME, request)

        val category = Category()
        val title: String? = request.getParameter("title")
        if (null != title && title.isNotBlank()) {
            category.title = title
            modelAndView.addObject("title", title)
        }
        category.status = StatusEnum.DELETE.key
        val page: Page<Category> = categoryService.list2page(super.initPage(request), category, Category.ID, false)!!
        modelAndView.addObject(VIEW_PAGE_NAME, page)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 恢复
     * @param request HttpServletRequest
     * @param id String
     * @return JSONObject
     */
    @RequiresPermissions(value = [RECOVER_URL])
    @RequestMapping("$RECOVER/{id}")
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_CATEGORY_RECOVER)
    @Throws(ControllerException::class)
    fun recover(@PathVariable(ID_NAME) id: String, request: HttpServletRequest): JSONObject? {
        if (super.isAjaxRequest(request)) {
            val idValue: BigInteger? = super.getId(id)
            // 验证数据
            if (null == idValue || BigInteger.ZERO > idValue) {
                super.initErrorCheckJSONObject()
            }
            val category: Category? = categoryService.getById(idValue!!)
            if (null == category) {
                super.initErrorCheckJSONObject()
            }
            // 恢复 Category id
            val backId: BigInteger? = categoryService.recoverBackId(category!!.id!!)
            return super.initRecoverJSONObject(backId)
        }
        return super.initReturnErrorJSONObject()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量恢复
     * @param request HttpServletRequest
     * @return JSONObject
     */
    @RequiresPermissions(value = [RECOVER_BATCH_URL])
    @RequestMapping(RECOVER_BATCH)
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_CATEGORY_RECOVER_BATCH)
    @Throws(ControllerException::class)
    fun recoverBatch(request: HttpServletRequest): JSONObject? {
        if (super.isAjaxRequest(request)) {
            val idArray: ArrayList
            <BigInteger>? = super.getIdArray(request)
            // 验证数据
            if (null == idArray || idArray.isEmpty()) {
                super.initErrorCheckJSONObject()
            }
            var flag = false
            if (null != idArray) {
                flag = categoryService.recoverBatch2UpdateStatus(idArray)!!
            }
            return if (flag) {
                super.initRecoverJSONObject(BigInteger.ONE)
            } else {
                super.initRecoverJSONObject(BigInteger.ZERO)
            }
        }
        return super.initReturnErrorJSONObject()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param request HttpServletRequest
     * @param category         Category
     * @return Category
     */
    private fun initAddData(request: HttpServletRequest, category: Category): Category {
        val addCategory: Category = Category.copy2New(category)
        // ip
        addCategory.gmtCreateIp = super.getIp(request)
        // 创建 Category 对象
        return categoryService.initSaveCategory(addCategory)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param category         Category
     * @return Category
     */
    private fun initEditData(category: Category): Category {
        val editCategory: Category = category
        // 创建 Category 对象
        return categoryService.initEditCategory(editCategory)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param category         Category
     * @param failResult ResultData
     * @return Boolean true(通过)/false(未通过)
     */
    private fun validationAddData(category: Category,
                                  failResult: ResultData):
            Boolean {
        val message: String
        // 是否存在
        val nameExist: Category? = categoryService.getByName(category
                .name!!.trim().toLowerCase())
        if (null != nameExist) {
            message = localeMessageSourceConfiguration.getMessage(BaseAdminController.MESSAGE_NAME_EXIST_NAME)
            failResult.setFail()[configProperties.messageName] = message
            return false
        }

        return true
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param category         Category
     * @param failResult ResultData
     * @return Boolean true(通过)/false(未通过)
     */
    private fun validationEditData(newCategory: Category, category: Category,
                                   failResult: ResultData): Boolean {
        val message: String
        // 是否存在
        val nameExist: Boolean = categoryService.propertyUnique(Category.NAME, newCategory
                .name!!.trim().toLowerCase(), category.name!!.toLowerCase())
        if (!nameExist) {
            message = localeMessageSourceConfiguration.getMessage(BaseAdminController.MESSAGE_NAME_EXIST_NAME)
            failResult.setFail()[configProperties.messageName] = message
            return false
        }

        val titleExist: Boolean = categoryService.propertyUnique(Category.TITLE, newCategory
                .title!!.trim().toLowerCase(), category.title!!.toLowerCase())
        if (!titleExist) {
            message = localeMessageSourceConfiguration.getMessage(BaseAdminController.MESSAGE_TITLE_EXIST_NAME)
            failResult.setFail()[configProperties.messageName] = message
            return false
        }
        return true
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到文章分类(小于3级)
     */
    private fun getCategory2Tree(request: HttpServletRequest) {
        // 权限 tree 数据
        val list: List<Category>? = categoryService.listTypeLt3()
        if (null != list) {
            request.setAttribute("treeCategory", list)
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End CategoryController class

/* End of file CategoryController.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/controller/cms/category/CategoryController.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// | ErYang出品 属于小极品 O(∩_∩)O~~ 共同学习 共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
