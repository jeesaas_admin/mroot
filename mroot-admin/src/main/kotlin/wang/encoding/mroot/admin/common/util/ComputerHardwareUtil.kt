/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.util


import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import wang.encoding.mroot.common.config.LocaleMessageSourceConfiguration
import java.net.UnknownHostException
import java.util.*

/**
 * 获得计算机硬件工具类
 *
 * @author ErYang
 */
@Component
class ComputerHardwareUtil {


    @Autowired
    private lateinit var localeMessageSourceConfiguration: LocaleMessageSourceConfiguration

    // -------------------------------------------------------------------------------------------------

    /**
     * 操作系统信息
     *
     * @return map 集合
     */
    fun osInfo(): Map<String, Any> {
        val map: MutableMap<String, Any> = mutableMapOf()
        val props: Properties = System.getProperties()
        try {
            map[localeMessageSourceConfiguration.getMessage("index.widget.osInfo")] = props
                    .getProperty("os.name")
            map[localeMessageSourceConfiguration.getMessage("index.widget.osInfo.version")] = props
                    .getProperty("os.version")
            map[localeMessageSourceConfiguration.getMessage("index.widget.osInfo.osArch")] = props
                    .getProperty("os.arch")
            map[localeMessageSourceConfiguration.getMessage("index.widget.osInfo.java.version")] = props
                    .getProperty("java.version")
            map[localeMessageSourceConfiguration.getMessage("index.widget.osInfo.java.specification" +
                    ".version")] = props.getProperty("java.specification.version")
            map[localeMessageSourceConfiguration.getMessage("index.widget.osInfo.java.vm.specification" +
                    ".version")] = props.getProperty("java.vm.specification.version")
            map[localeMessageSourceConfiguration.getMessage("index.widget.osInfo.java.class" +
                    ".version")] = props.getProperty("java.class.version")
        } catch (e: UnknownHostException) {
            e.printStackTrace()
        }
        return map
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ComputerHardwareUtil class

/* End of file ComputerHardwareUtil.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/util/ComputerHardwareUtil.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
