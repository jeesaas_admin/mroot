/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------

package wang.encoding.mroot.admin.controller.language


import com.alibaba.fastjson.JSONObject
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.LocaleResolver
import org.springframework.web.servlet.support.RequestContextUtils
import wang.encoding.mroot.common.annotation.RequestLogAnnotation
import wang.encoding.mroot.common.constant.RequestLogConstant
import wang.encoding.mroot.common.exception.ControllerException
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * 语言控制器
 *
 * @author ErYang
 */
@RestController
class LanguageController {

    companion object {
        private const val ZH = "zh"
        private const val CN = "CN"
        private const val EN = "en"
        private const val US = "US"
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 改变语言
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @param lang 语言
     * @return JSONObject
     */
    @RequestMapping("/language/{lang}")
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CHANGE_LANGUAGE_VIEW)
    @Throws(ControllerException::class)
    fun changeLanguage(request: HttpServletRequest,
                       response: HttpServletResponse, @PathVariable("lang") lang: String): JSONObject {
        val result = JSONObject()
        val localeResolverOptional: Optional<LocaleResolver> = Optional.ofNullable(RequestContextUtils.getLocaleResolver(request))
        if (localeResolverOptional.isPresent) {
            val localeResolver: LocaleResolver = localeResolverOptional.get()
            localeResolver.resolveLocale(request).language
            when {
                ZH == lang -> {
                    localeResolver.setLocale(request, response, Locale(ZH, CN))
                }
                EN == lang -> {
                    localeResolver.setLocale(request, response, Locale(EN, US))
                }
                else -> {
                    localeResolver.setLocale(request, response, Locale(ZH, CN))
                }
            }
        }
        return result
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End LanguageController class

/* End of file LanguageController.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/controller/language/LanguageController.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
