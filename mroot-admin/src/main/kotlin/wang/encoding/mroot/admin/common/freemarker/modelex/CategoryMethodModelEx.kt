/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.freemarker.modelex

import freemarker.template.TemplateMethodModelEx
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import wang.encoding.mroot.model.entity.cms.Category
import wang.encoding.mroot.service.cms.CategoryService
import java.math.BigInteger


/**
 * Category  freemarker 方法类
 *
 * @author ErYang
 */
@Component
class CategoryMethodModelEx
@Autowired constructor(private var categoryService: CategoryService) : TemplateMethodModelEx {


    /**
     * logger
     */
    private val logger: Logger = LoggerFactory.getLogger(CategoryMethodModelEx::class.java)


    override fun exec(list: List<*>): Category? {
        var category: Category? = null
        val id: BigInteger? = list[0].toString().toBigIntegerOrNull()
        if (null != id) {
            category = categoryService.getById(id)
        }
        return if (null != category) {
            category
        } else {
            category = categoryService.getById(1.toBigInteger())
            category
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End CategoryMethodModelEx class

/* End of file CategoryMethodModelEx.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/freemarker/modelex/CategoryMethodModelEx.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
