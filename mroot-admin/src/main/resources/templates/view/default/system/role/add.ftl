<@OVERRIDE name="MAIN_CONTENT">

    <#include "/default/common/pageAlert.ftl">

<#-- 内容开始 -->
<div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    ${I18N("message.form.head.title")}
                </h3>
            </div>
        </div>
    </div>

<#-- 表单开始 -->
    <form class="m-form m-form--state m-form--fit m-form--label-align-right" id="role_form" action="${save}"
          method="post" autocomplete="off">
        <div class="m-portlet__body">

            <#include "/default/common/formAlert.ftl">

            <@formTokenAndRefererUrl></@formTokenAndRefererUrl>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.name")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="name"
                           placeholder="${I18N("message.form.name.text")}"
                           value="${role.name}"
                    >
                    <span class="m-form__help">${I18N("message.form.name.text.help")}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.title")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="title"
                           placeholder="${I18N("message.form.title.text")}"
                           value="${role.title}"
                    >
                    <span class="m-form__help">${I18N("message.form.title.text.help")}</span>
                </div>
            </div>

            <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.sort")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="sort" id="sort"
                           placeholder="${I18N("message.form.sort.text")}"
                           value="${role.sort}"
                    >
                    <span class="m-form__help">${I18N("message.form.sort.text.help")}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.remark")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                   <textarea class="form-control" rows="3" name="remark" id="remark"
                             placeholder="${I18N("message.form.remark.text")}"
                   >${role.remark}</textarea>
                    <span class="m-form__help">${I18N("message.form.remark.text.help")}</span>
                </div>
            </div>


        </div>

    <#-- 提交按钮 -->
               <@formOperate></@formOperate>

    </form>
<#-- 表单结束 -->

</div>
<#-- 内容结束 -->

</@OVERRIDE>
<@OVERRIDE name="PAGE_MESSAGE">
    <#include "/default/system/role/roleJs.ftl">
</@OVERRIDE>
<#include "/default/scriptPlugin/form.ftl">
<@OVERRIDE name="CUSTOM_SCRIPT">
<script>
    jQuery(document).ready(function () {
        // 顶部导航高亮
        Tool.highlight_top_nav('${navIndex}');
        FormValidation.formValidationRole();

        Tool.highlightTouchSpin('#sort', ${maxSort});

        autosize($('#remark'));

    });
</script>
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
